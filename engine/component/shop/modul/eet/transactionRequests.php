<?php

use Nette\Forms\Form;
use Nette\Utils\Strings;

$_tab_link = sprintf('%s&amp;page=%d&amp;edit=%d&amp;submenu=transactionRequests', $_internal_link, $_page, $engine->shop->eet->id);

/**
 * Transaction call detail
 */
if (isset($_GET['callDetail'])) {
	$idCall = $_GET['callDetail'];
	$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', 'transactionRequest.callDetail'));
	$template->prepare();

	$engine->shop->eet->call->load(['id' => $idCall]);

	$template->assign('STATE', $engine->shop->eet->call->state);
	$template->assign('CREATED', $engine->shop->eet->call->created);
	$template->assign('WARNING', $engine->shop->eet->call->warning);
	$template->assign('MESSAGE', $engine->shop->eet->call->message);
	$template->assign('REQUEST', $engine->shop->eet->call->request);
	$template->assign('RESPONSE', $engine->shop->eet->call->response);

	$states = \Redenge\EET\State::getList();
	$states[''] = 'Neznámý';

	$form = new Form;
	$form->addSelect('state', _('Stav'), $states)
		->setDisabled();
	$form->addText('created', _('Vytvořeno'))
		->setDisabled();
	$form->addCheckbox('warning', _('Varování'))
		->setDisabled();
	$form->addTextArea('message', _('Zpráva'))
		->setAttribute('cols', 100)
		->setAttribute('rows', 5)
		->setDisabled();
	$form->addTextArea('request', _('Požadavek'))
		->setAttribute('cols', 100)
		->setAttribute('rows', 30)
		->setDisabled();
	$form->addTextArea('response', _('Odpověď'))
		->setAttribute('cols', 100)
		->setAttribute('rows', 30)
		->setDisabled();

	$form->setDefaults($engine->shop->eet->call->getArray());

	$template->assign('FORM', (string) $form);

	$templateLevel2->assign('BODY', $template->getOutputContent());
	return;
}

$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', 'simpleList'));
$template->prepare();

/**
 * Transaction call list
 */
$args = [];
$filters = [];
$table = new t_table('eet_transaction_call', $engine->admin, _edit_right(), $_component_name);

$table->create_text_field('detail', _('Detail'));
$table->create_text_field('state', _('Stav'));
$table->create_date_field('created', _('Vytvořeno'));
$table->create_bool_field('warning', _('Upozornění'));
$table->create_text_field('message', _('Message'));
$table->create_text_field('request', _('Request'));
$table->create_text_field('response', _('Response'));

$table->set_sort_fields('state', 'created', 'request', 'response', 'warning');

$table->register_searchbox_text($filters, 'state', '', $args);
$table->register_searchbox_date($filters, 'created', 'DATE(created)', $args);
$table->register_searchbox_bool($filters, 'warning', '', $args);
$table->register_searchbox_text($filters, 'message', '', $args);
$table->register_searchbox_text($filters, 'request', '', $args);
$table->register_searchbox_text($filters, 'response', '', $args);

$table->add_header(sprintf('%s&amp;page=%d', $_tab_link, $_page));

$filters[] = sprintf('id_eet_transaction = %d', $engine->shop->eet->id);
$records = $engine->shop->eet->call->getPagedRecords($_page, $_limit, $_total, '*', NULL, $table->getSort(), implode(' AND ', $filters), NULL, NULL, $args);

while ($row = mysqlc_fetch_assoc($records)) {
	$table->detail = sprintf('<a href="%s" title="%s"><img src="/admin/images/%s" alt=""></a>', sprintf('%s&amp;callDetail=%d', $_tab_link, $row['id']), 'Detail', 'search_expand.png');
	$table->created = htmlspecialchars($row['created']);
	$table->state = htmlspecialchars($row['state']);
	$table->warning = $row['warning'];
	$table->message = $row['message'];
	$table->request = htmlspecialchars(Strings::truncate($row['request'], 60));
	$table->response = htmlspecialchars(Strings::truncate($row['response'], 60));

	$table->add_line($row['state'] === \Redenge\EET\State::SUCCESS ? 'c7ff00' : NULL); // OK color
}

mysqlc_free_result($records);
$table->add_to_template($template);

_add_pager($template, $_page, $_total, $_limit, $_internal_link, $_strankovani_pole);

$templateLevel2->assign('BODY', $template->getOutputContent());
