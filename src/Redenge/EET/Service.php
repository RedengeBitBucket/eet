<?php

namespace Redenge\EET;

use Nette\Utils\Validators;
use Ondrejnov\EET\Dispatcher;
use Ondrejnov\EET\Receipt;
use Ondrejnov\EET\Exceptions\ServerException;
use Redenge\EET\Model\Transaction;

/**
 * @implements IService
 */
class Service extends \Nette\Object implements IService
{
	/** @var \DBObject */
	private $transactions;

	/** @var Settings */
	private $settings;

	/** @var IDispatcher */
	private $dispatcher;

	/** @var \Swift_Mailer */
	private $mailer;

	/**
	 * @param \DBObject  $transactions
	 * @param Settins    $settings
	 */
	public function __construct(\DBObject $transactions, Settings $settings)
	{
		$this->transactions = $transactions;
		$this->settings = $settings;

		// \Tracy\Debugger::$maxLength = 6000;
		// \Tracy\Debugger::$maxDepth = 5;
		// \Tracy\Debugger::$showLocation = TRUE;
	}

	/**
	 * @param Transaction $transaction
	 *
	 * @return bool
	 */
	public function save(Transaction $transaction)
	{
		if (empty($transaction->getDicPopl())) {
			$transaction->setDicPopl($this->settings->getDicPopl());
		}

		if (empty($transaction->getDicPoverujiciho())) {
			$transaction->setDicPoverujiciho($this->settings->getDicPoverujiciho());
		}

		if (empty($transaction->getIdProvoz())) {
			$transaction->setIdProvoz($this->settings->getIdProvoz());
		}

		if (empty($transaction->getIdPokl())) {
			$transaction->setIdPokl($this->settings->getIdPokl());
		}

		/**
		 * @Wincor Nixdorf API should return BKP and PKP codes, we can't create them due certs
		 */
		$codes = $this->getDispatcher()->getCheckCodes($transaction);
		if ($codes !== FALSE) {
			$transaction->setBkp($codes['bkp']);
			$transaction->setPkp($codes['pkp']);
		}

		$transaction->setOvereni($this->settings->isOvereni());
		$transaction->setRezim($this->settings->isRezim());

		return $this->transactions->persist($transaction);
	}

	/**
	 * Process transaction (used in cron jobs etc.)
	 *
	 * @return mixed
	 */
	public function sendTransactions()
	{
		if ($this->settings->getCronStatus() !== CronStatus::READY && $this->settings->getCronStatus() !== CronStatus::ERROR) {
			$message = sprintf('%s may be already running, check status or cron status file', __FUNCTION__);
			if (defined('STDIN')) {
				echo $message . "\n";
			} else {
				bdump($message);
			}

			return 1;
		}

		/**
		 * @cron Set status as RUNNING
		 */
		$this->settings->setCronStatus(CronStatus::RUNNING);

		try {
			$counter = 0;
			$records = $this->transactions->getRecords('*', NULL, 'id DESC', sprintf('(state = \'%s\' OR state = \'%s\') AND counter < %d', State::CREATED, State::TIMEOUT, $this->settings->getMaxSendAttempts()));
			while ($row = mysqlc_fetch_assoc($records)) {
				$transaction = $this->transactions->get((object) $row);
				bdump([
					'transaction' => $transaction,
					'row' => $row,
				]);
				$status = $this->send($transaction);
				$counter++;
			}
			mysqlc_free_result($records);

			// @todo Dump time and stats
			bdump(['counter' => $counter]);

			/**
			 * @cron Set status as READY
			 */
			$this->settings->setCronStatus(CronStatus::READY);
		} catch (\Exception $e) {
			/**
			 * @cron Set status as ERROR
			 */
			$this->settings->setCronStatus(CronStatus::ERROR);
		}
	}

	/**
	 * Send transaction immediately, basically only on checkout
	 *
	 * @param int $idTransaction
	 */
	public function sendTransactionById($idTransaction)
	{
		$this->send($this->transactions->get($idTransaction));
	}

	/**
	 * @param $transaction Transaction
	 *
	 * @return bool
	 */
	public function sendEmailNotification(Transaction $transaction)
	{
		if ($this->settings->isNoEmailSend()) {
			return FALSE;
		}
		$pdfReceipt = $this->getPdfReceipt($transaction);
		$translator = new StaticTranslator($transaction->getLanguage());
		bdump(['pdf' => $pdfReceipt]);

		$message = \Swift_Message::newInstance()
			->setSubject($translator->translate('email.subject'))
			->setFrom([$this->settings->getEmail()])
			->setTo([$transaction->email])
			->setBody($translator->translate('email.body'))
			->attach(new \Swift_Attachment($pdfReceipt, $transaction->getFileName(), 'application/pdf'));

		$status = $this->getMailer()->send($message);
		bdump($status);

		return $status;
	}

	/**
	 * @param Transaction    $transaction
	 *
	 * @return HtmlReceipt
	 */
	public function getHtmlReceipt(Transaction $transaction)
	{
		if ($transaction->getId() === 0) {
			return FALSE;
		}

		return new HtmlReceipt($transaction, $this->getTemplate($transaction->getLanguage() ?: Settings::LANGUAGE_DEFAULT));
	}

	/**
	 * @param Transaction    $transaction
	 * @param bool           $outputToBrowser
	 *
	 * @return NULL|string
	 */
	public function getPdfReceipt(Transaction $transaction, $outputToBrowser = FALSE)
	{
		if (!($htmlReceipt = $this->getHtmlReceipt($transaction))) {
			return FALSE;
		}

		$pdfReceipt = new \mPDF();
		$pdfReceipt->writeHtml((string) $htmlReceipt);


		return $pdfReceipt->output($transaction->getFileName(), $outputToBrowser ? 'D' : 'S');
	}

	/**
	 * @return string
	 */
	public function getCertDir()
	{
		return $this->settings->getCertDir();
	}

	/**
	 * @return array
	 */
	public function getLanguages()
	{
		return $this->settings->getLanguages();
	}

	/**
	 * @param Transaction $transaction
	 */
	private function send(Transaction $transaction)
	{
		$dispatcher = $this->getDispatcher();

		/**
		 * Chcek if BKP and PKP is generated, probably new "storno" transaction will not have BKP and PKP, so we have to add them
		 * @Wincor Nixdorf API should return BKP and PKP codes itself, we can't create them due only WNI has private key
		 */
		$codes = $this->getDispatcher()->getCheckCodes($transaction);
		if ($codes !== FALSE && empty($transaction->getBkp()) || empty($transaction->getPkp())) {
			$transaction->setBkp($codes['bkp']);
			$transaction->setPkp($codes['pkp']);
		}

		/**
		 * Send transaction via dispatcher
		 */
		$transaction = $dispatcher->send($transaction);
		bdump(['response t' => $transaction]);

		/**
		 * Update Transaction
		 * Save call (request, response, ...)
		 */
		$transaction->setPrvniZaslani(FALSE);
		$transaction->setCounter($transaction->getCounter() + 1);
		$transaction->setDatOdesl((new \DateTime('now',new \DateTimeZone('Europe/Prague')))->format('Y-m-d H:i:s'));
		if (Validators::isEmail($transaction->getEmail()) && $transaction->isSendable() && $transaction->getNotifySent() === 0 && $this->sendEmailNotification($transaction)) {
			$transaction->setNotifySent($transaction->getNotifySent() + 1);
		}

		$this->transactions->persist($transaction);

		$this->transactions->call->reset();
		$this->transactions->call->created = $transaction->getDatOdesl();
		$this->transactions->call->state = $transaction->getState();
		$this->transactions->call->request = $dispatcher->getLastRequest();
		$this->transactions->call->response = $dispatcher->getLastResponse();
		$this->transactions->call->warning = isset($response->Varovani);
		$this->transactions->call->message = $transaction->call->message ?: '';
		$this->transactions->call->id_eet_transaction = $transaction->getId();
		$this->transactions->call->save();

		// bdump([
		// 	'request' => $dispatcher->getLastRequest(),
		// 	'response' => $dispatcher->getLastResponse(),
		// ]);
	}

	/**
	 * @return Dispatcher
	 */
	private function getDispatcher()
	{
		if ($this->dispatcher == NULL) {
			if ($this->settings->getDispatcher() == Settings::MF_DISPATCHER) {
				$this->dispatcher = new MfDispatcherAdapter($this->settings);
			} elseif ($this->settings->getDispatcher() == Settings::WNI_DISPATCHER) {
				$this->dispatcher = new WniDispatcher($this->settings);
			} else {
				throw new \Exception(sprintf('Dispatcher (provider) "%s" isn\'t supported', $this->settings->getDispatcher()));
			}
		}

		return $this->dispatcher;
	}

	/**
	 * @return \Swift_Mailer
	 */
	private function getMailer()
	{
		if ($this->mailer === NULL) {
			$transport = \Swift_MailTransport::newInstance();
			$this->mailer = \Swift_Mailer::newInstance($transport);
		}

		return $this->mailer;
	}

	/**
	 * @param    $language
	 *
	 * @return string|bool
	 */
	private function getTemplate($language)
	{
		return isset($this->settings->getTemplates()[$language]) ? $this->settings->getTemplates()[$language] : FALSE;
	}
}


interface IService
{
	/** @param Transaction $transaction */
	function save(Transaction $transaction);

	/** @param */
	function sendTransactions();

	/** @param int $idTransaction */
	function sendTransactionById($idTransaction);

	/** @param Transaction $transaction */
	function sendEmailNotification(Transaction $transaction);

	/** @param Transaction $transaction */
	function getHtmlReceipt(Transaction $transaction);

	/**
	 * @param Transaction    $transaction
	 * @param bool           $outputToBrowser
	 */
	function getPdfReceipt(Transaction $transaction, $outputToBrowser = FALSE);

	/**
	 * @return array
	 */
	function getLanguages();
}
