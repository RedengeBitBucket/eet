<?php

namespace Redenge\EET\Model;

/**
 * Stored APIs calls
 */
class Call extends \DBObject
{
	public function __construct($db, $parentNode = NULL)
	{
		parent::__construct($db, 'eet_transaction_call', $parentNode);

		parent::createString('state');
		parent::createDate('created', (new \DateTime())->format('Y-m-d H:i:s'));
		parent::createString('request');
		parent::createString('response');
		parent::createBool('warning', 0);
		parent::createString('message');

		parent::createInteger('id_eet_transaction');
	}
}
