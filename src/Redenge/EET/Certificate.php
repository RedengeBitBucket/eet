<?php

namespace Redenge\EET;

class Certificate
{
	const   KEY_NAME = 'eet.key',
			CERT_NAME = 'eet.pem';

	/** @var string */
	private $pkey;

	/** @var string */
	private $cert;

	/**
	 * @param string    $certificate
	 * @param string    $password
	 */
	public function __construct($certificate, $password)
	{
		$certs = [];
		$pkcs12 = $certificate;
		if (!extension_loaded('openssl') || !function_exists('openssl_pkcs12_read')) {
			throw new \Exception('OpenSSL extension is not available');
		}

		$openSSL = openssl_pkcs12_read($pkcs12, $certs, $password);
		if (!$openSSL) {
			throw new \Exception('Unable to export certificates');
		}

		$this->pkey = $certs['pkey'];
		$this->cert = $certs['cert'];
	}

	/**
	 * @return string
	 */
	public function getPrivateKey()
	{
		return $this->pkey;
	}

	/**
	 * @return string
	 */
	public function getCert()
	{
		return $this->cert;
	}
}
