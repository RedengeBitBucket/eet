<?php

use Nette\Forms\Form;
use Redenge\Admin\Module\EET;
use Redenge\EET\Certificate;

$privateKeyPath = sprintf('%s/%s', $service->getCertDir(), Certificate::KEY_NAME);
$certPath = sprintf('%s/%s', $service->getCertDir(), Certificate::CERT_NAME);
$status = file_exists($privateKeyPath) && file_exists($certPath);
$currentLink = htmlspecialchars_decode(sprintf('%s&amp;page=%d', $_internal_link, $_page));

$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', basename(__FILE__, '.php')));
$template->prepare();

// if ($status) {
// 	$deleteForm = new Form;
// 	$deleteForm->addSubmit('send', _('Smazat'));

// 	if ($deleteForm->isSuccess()) {
// 		bdump('Mažu cert');
// 		$engine->redirect($currentLink);
// 	}
// }

$form = new Form;
$form->addUpload('certificate', _('Certifikát'))
	->addRule(Form::MIME_TYPE, _('Certifikát musí být být ve formátu p12'), 'application/octet-stream')
	->setRequired();
$form->addText('password', _('Heslo'));

$form->addSubmit('send', _('Nainstalovat'));

/**
 * Odeslání formuláře
 */
if ($form->isSuccess()) {
	$values = $form->getValues();

	bdump($values->certificate->getContentType());
	try {
		$certificate = new Certificate($values->certificate->getContents(), $values->password);
		file_put_contents($privateKeyPath, $certificate->getPrivateKey());
		file_put_contents($certPath, $certificate->getCert());
		$engine->redirect($currentLink);
	} catch (\Exception $e) {
		$form->addError($e->getMessage());
	}
}

$template->assign('FORM', (string) $form);
$template->newBlock($status ? 'installed' : 'not-installed');
// $template->assign('DELETE_FORM', $status ? (string) $deleteForm : '');

$templateLevel1->assign('BODY', $template->getOutputContent());
