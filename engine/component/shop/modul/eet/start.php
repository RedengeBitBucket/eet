<?php

/**
 * @EET
 */
if (defined('ENGINE_VERSION') && ENGINE_VERSION <= 50200) {
	$engine->shop->eet = new \Redenge\EET\Model\Transaction($database);
}
$settings = new Redenge\EET\Settings($engine->shop->settings->getByTheme(\Redenge\Admin\Module\EET::SETTINGS_THEME));
$service = new Redenge\EET\Service($engine->shop->eet, $settings);

/**
 * Compatibility
 */
if ($engine->shop->settings instanceof \persistor) {
	$resetMethodName = 'clear';
	$themeColumnName = 'id_settings_theme';
	$loadMethodName = 'load_by';
	$orderIndexColumnName = 'order_number';
} else {
	$resetMethodName = 'reset';
	$themeColumnName = 'theme';
	$loadMethodName = 'load';
	$orderIndexColumnName = 'index';
}

$templateLevel1 = new TemplatePower(ENGINE_PATH . '/lib/templates/start.tpl');
$templateLevel1->prepare();

$tabsLevel1 = [];

$_jazyky = [];
$recordset = $engine->shop->settings_interface->language->fw_dataset(['orderkoef', 'name ASC'], 'DESC');

while ($row = mysqlc_fetch_assoc($recordset)) {
	array_push($_jazyky, $row);
}
mysqlc_free_result($recordset);

$tabsLevel1['transactionList'] = _('Transakce');
if (\Tracy\Debugger::isEnabled() && !\Tracy\Debugger::$productionMode) {
	$tabsLevel1['config'] = _('Nastavení');
	$tabsLevel1['receipt'] = _('Účtenka');
	$tabsLevel1['certificate'] = _('Certifikát');
	$tabsLevel1['changelog'] = _('Changelog');
	$tabsLevel1['playground'] = _('Playground');
}

if ($_menu == '') {
	list($_menu, $tmp) = each($tabsLevel1);
}

$_special_parametr = '';
$_special_title = '';
$_location = [];

if (array_key_exists($_menu, $tabsLevel1)) {
	array_push($_location, $tabsLevel1[$_menu]);
}

$file = sprintf('%s/%s.php', __DIR__, $_menu);

if (!file_exists($file)) {
	throw new Exception("FATAL ERROR! Warning: Cannot find $file");
}

include($file);

$templateLevel1->assign('TITLE', $_actual_module_name);
$templateLevel1->assign('ZALOZKY', _get_master_bookmarks($_modul_link, $tabsLevel1, $_menu));
$templateLevel1->assign('LOCATION', join($_location, ' &raquo; '));
$templateLevel1->assign('FORM_ACTION', sprintf('%s&amp;page=%d%s', $_internal_link, $_page, $_special_parametr));

/**
 * Render module template
 */
_add_content($templateLevel1->getOutputContent());
