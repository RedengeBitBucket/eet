<?php

namespace Redenge\Admin\Module;

use Redenge\Admin\IModulePackage;
use Redenge\Admin\Model;
use Redenge\Admin\Module;

class EET implements \Redenge\Admin\IModulePackage
{
	const SETTINGS_THEME = 1010;

	/**
	 * @return array
	 */
	public function getModels()
	{
		return [
			new Model('eet', 'Redenge\EET\Model\Transaction'),
		];
	}

	/**
	 * @return array
	 */
	public function getModules()
	{
		return [
			new Module('shop', 'eet', 'EET', __DIR__ . '/../../../engine'),
		];
	}
}
