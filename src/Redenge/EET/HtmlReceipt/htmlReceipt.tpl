<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
	<title></title>
	<meta name="description" content="" />
	<meta name="viewport" content="width=device-width" />
	<style type='text/css'>
		.uctenka {
			width: 70mm;
			padding: 5mm;
			background: white;
		}
		dl {
			margin: 10 0 10 0;
			overflow-wrap: break-word;
			word-wrap: break-word;
			-ms-word-break: break-all;
			word-break: break-all;
			word-break: break-word;
			-ms-hyphens: auto;
			-moz-hyphens: auto;
			-webkit-hyphens: auto;
			hyphens: auto;
		}
		dl dd {
			display: inline;
			margin: 0;
		}
		dl dd:after{
			display: block;
			content: '';
		}
		dl dt{
			display: inline-block;
			min-width: 100px;
			font-weight: bold;
		}
	</style>
</head>
<body>
	{BODY}
</body>
</html>