<?php

namespace Redenge\EET\Model;

interface ITransaction
{
	function getState();

	function getEmail();

	function getUuidZpravy();

	function getDatOdesl();

	function getPrvniZaslani();

	function getOvereni();

	function getDicPopl();

	function getDicPoverujiciho();

	function getIdProvoz();

	function getIdPokl();

	function getPoradCis();

	function getDatTrzby();

	function getCelkTrzba();

	function getZaklNepodlDph();

	function getZaklDan1();

	function getDan1();

	function getZaklDan2();

	function getDan2();

	function getZaklDan3();

	function getDan3();

	function getCestSluz();

	function getPouzitZboz1();

	function getPouzitZboz2();

	function getPouzitZboz3();

	function getUrcenoCerpZuct();

	function getCerpZuct();

	function getRezim();

	function getPkp();

	function getBkp();

	function getDatPrij();

	function getFik();

	function getTest();

	function getCounter();

	function getNotifySent();

	function getIdOrder();

	function getIdBill();

	//

	function getSecondaryCode();

	function getSecondaryCodeName();

	function getSecondaryCode();
}
