<?php

namespace Redenge\EET;

use Redenge\EET\Model\Transaction;

class HtmlReceipt
{
	const   NAME_KEY = 'name',
			METHOD_KEY = 'method';

	/** @var \TemplatePower */
	private $template;

	/** @var Transaction */
	private $transaction;

	/** @var string */
	private $body;

	/**
	 * @param Transaction    $transaction
	 * @param string|NULL    $template
	 */
	public function __construct(Transaction $transaction, $template = NULL)
	{
		$this->transaction = $transaction;
		$this->body = $template ?: file_get_contents(__DIR__ . '/default.tpl');
		$this->template = new \TemplatePower(__DIR__ . '/htmlReceipt.tpl');
		$this->template->prepare();

		bdump(['body' => $this->body, 'template' => $template]);
	}

	/**
	 * @return string
	 */
	public function __toString()
	{
		try {
			/**
			 * Fill placeholders in template
			 */
			foreach (self::getAvailableVariables() as $key => $info) {
				$this->body = preg_replace('/{' . $key . '}/', $this->transaction->{$info[self::METHOD_KEY]}(), $this->body);
			}

			$this->template->assign('BODY', $this->body);

			return $this->template->getOutputContent();
		} catch (\Exception $e) {
			bdump(['Chyba' => $e->getMessage()]);
			return sprintf('Chyba: %s', $e->getMessage());
		}
	}

	/**
	 * @todo Nějak umravnit, pole fuj o_O
	 * @return array
	 */
	public static function getAvailableVariables()
	{
		return [
			'DIC_POPL' => [
				self::NAME_KEY => 'DIČ poplatníka',
				self::METHOD_KEY => 'getDicPopl',
			],
			'REZIM' => [
				self::NAME_KEY => 'Režim',
				self::METHOD_KEY => 'getFormattedRezim',
			],
			'ID_PROVOZ' => [
				self::NAME_KEY => 'ID Provozovny',
				self::METHOD_KEY => 'getIdProvoz',
			],
			'ID_POKL' => [
				self::NAME_KEY => 'ID Pokladny',
				self::METHOD_KEY => 'getIdPokl',
			],
			'PORAD_CIS' => [
				self::NAME_KEY => 'Pořadové číslo',
				self::METHOD_KEY => 'getPoradCis',
			],
			'DATUM' => [
				self::NAME_KEY => 'Datum',
				self::METHOD_KEY => 'getSecondaryCodeDate',
			],
			'BKP' => [
				self::NAME_KEY => 'BKP',
				self::METHOD_KEY => 'getBkp',
			],
			'SECONDARY_CODE_NAME' => [
				self::NAME_KEY => 'Název pro FIC nebo PKP',
				self::METHOD_KEY => 'getSecondaryCodeName',
			],
			'SECONDARY_CODE' => [
				self::NAME_KEY => 'FIC nebo PKP',
				self::METHOD_KEY => 'getSecondaryCode',
			],
			'CELK_TRZBA' => [
				self::NAME_KEY => 'Celková tržba v CZK',
				self::METHOD_KEY => 'getCelkTrzba',
			],
			// @todo Zbytek cen
		];
	}
}
