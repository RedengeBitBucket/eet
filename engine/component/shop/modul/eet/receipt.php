<?php

use Nette\Forms\Form;
use Nette\Utils\Html;
use Nette\Utils\Strings;
use Redenge\Admin\Module\EET;

$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', basename(__FILE__, '.php')));
$template->prepare();

$translates = [
	'cs' => 'Čeština',
	'cz' => 'Čeština',
	'sk' => 'Slovenština',
	'en' => 'Angličtina',
	'de' => 'Němčina',
	'pl' => 'Polština',
];

$languages = $service->getLanguages();

$form = new Form;
foreach ($languages as $lang) {
	$form->addTextArea(sprintf('eet_template_%s', $lang), isset($translates[$lang]) ? $translates[$lang] : Strings::upper($lang))
		->setAttribute('class', 'editor');
}

$form->addSubmit('send', _('Uložit'));

/**
 * Výchozí hodnoty
 */
$defaults = [];
$records = $engine->shop->settings->getRecords('*', NULL, NULL, sprintf('%s = %d', $themeColumnName, \Redenge\Admin\Module\EET::SETTINGS_THEME));
while ($row = mysqlc_fetch_assoc($records)) {
	$defaults[$row['code']] = $row['setting'];
}
mysqlc_free_result($records);

$form->setDefaults($defaults);

/**
 * Odeslání formuláře
 */
if ($form->isSuccess()) {
	$values = $form->getValues();

	foreach ($values as $key => $value) {
		$engine->shop->settings->{$loadMethodName}([
			'code' => $key,
			$themeColumnName => EET::SETTINGS_THEME,
		]);

		if ($engine->shop->settings->id === 0) {
			$engine->shop->settings->{$resetMethodName}();
			$engine->shop->settings->code = $key;
			$engine->shop->settings->{$themeColumnName} = EET::SETTINGS_THEME;
			$engine->shop->settings->type = 'text'; // @todo
		}

		$engine->shop->settings->setting = $value;
		$engine->shop->settings->save();
	}
}

$template->assign('FORM', (string) $form);

/**
 * Dostupné proměnné
 */
foreach (\Redenge\EET\HtmlReceipt::getAvailableVariables() as $key => $info) {
	$template->newBlock('variables');
	$template->assign('VARIABLE', $key);
	$template->assign('NAME', $info[\Redenge\EET\HtmlReceipt::NAME_KEY]);
}

$templateLevel1->assign('BODY', $template->getOutputContent());
