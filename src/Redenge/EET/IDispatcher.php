<?php

namespace Redenge\EET;

use Ondrejnov\EET\Receipt;
use Redenge\EET\Model\Transaction;

interface IDispatcher
{
	/**
	 * @param Transaction    $transaction
	 *
	 * @return Transaction
	 */
	function send(Transaction $transaction);

	/**
	 * @param Transaction $transaction
	 *
	 * @return array
	 */
	function getCheckCodes(Transaction $transaction);

	/**
	 * @return string
	 */
	function getLastRequest();

	/**
	 * @return string
	 */
	function getLastResponse();
}
