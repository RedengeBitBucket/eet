<?php

namespace Redenge\EET;

use Nette\Utils\Strings;

class Settings extends \Nette\Object
{
	const   MF_DISPATCHER = 'mf',
			WNI_DISPATCHER = 'wni',
			LANGUAGE_DEFAULT = 'cs',
			LANGUAGE_KEY = 'eet_languages',
			CURRENCY_DEFAULT = 'CZK';

	/** @var string */
	private $dispatcher;

	/** @var int */
	private $max_send_attempts;

	/** @var bool */
	private $active;

	/** @var bool */
	private $production;

	/** @var bool */
	private $overeni;

	/** @var bool */
	private $no_email_send;

	/** @var string */
	private $dic_popl;

	/** @var string */
	private $dic_poverujiciho;

	/** @var string */
	private $id_provoz;

	/** @var string */
	private $id_pokl;

	/** @var bool */
	private $rezim;

	/** @var string */
	private $email;

	/** @var string */
	private $cert;

	/** @var string */
	private $password;

	/** @var string */
	private $api_production;

	/** @var string */
	private $api_development;

	/**
	 * @var string
	 * @deprecated
	 */
	private $receipt_header;

	/**
	 * @var string
	 * @deprecated
	 */
	private $receipt_footer;

	/** @var string */
	private $certDir;

	/** @var string|array */
	private $languages;

	/** @var string */
	private $cronStatus;

	/** @var array */
	private $templates = [];

	/** @var string */
	private $cronStatusFile;

	/** @var string */
	private $cronStatusFileUnsafe;

	/**
	 * @param array     $configuration
	 * @param string    $certDir
	 * @param string    $cronStatusFile
	 */
	public function __construct(array $configuration, $certDir = APP_DIR . '/cert/eet', $cronStatusFile = APP_DIR . '/temp/eet_cron_status')
	{
		\Nette\Utils\SafeStream::register();

		/**
		 * Get settings and templates
		 */
		foreach ($configuration as $key => $value) {
			$col = Strings::substring($key, 4, Strings::length($key) + 1);
			if (Strings::startsWith($col, 'template_')) {
				$lang = Strings::substring($col, 9, Strings::length($col) + 1);
				bdump([$key => $lang]);
				/**
				 * Template
				 */
				$this->templates[$lang] = $value;
			} else {
				/**
				 * Settings
				 */
				$this->{$col} = $value;
			}
		}

		if ($this->receipt_header !== NULL || $this->receipt_footer !== NULL) {
			trigger_error(sprintf('Remove rows with code = \'eet_receipt_header\' and \'eet_receipt_footer\' from settings table', __CLASS__, __FUNCTION__), E_USER_DEPRECATED);
		}

		$this->certDir = $certDir;

		$this->cronStatusFile = 'nette.safe://' . $cronStatusFile;
		$this->cronStatusFileUnsafe = $cronStatusFile;

		/**
		 * @Cron Status File
		 */
		if (!file_exists($this->cronStatusFile)) {
			file_put_contents($this->cronStatusFile, CronStatus::READY);
			chmod($this->cronStatusFileUnsafe, 0777);
			$cronStatus = CronStatus::READY;
		} else {
			$cronStatus = file_get_contents($this->cronStatusFile) ?: CronStatus::READY;
		}

		$this->cronStatus = $cronStatus;
	}

	/**
	 * @return boolean
	 */
	public function isActive()
	{
		return $this->active;
	}

	/**
	 * @return string
	 */
	public function getCert()
	{
		return $this->cert;
	}

	/**
	 * @return string
	 */
	public function getDicPopl()
	{
		return $this->dic_popl;
	}

	/**
	 * @return string
	 */
	public function getDicPoverujiciho()
	{
		return $this->dic_poverujiciho;
	}

	/**
	 * @return string
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @return string
	 */
	public function getIdPokl()
	{
		return $this->id_pokl;
	}

	/**
	 * @return string
	 */
	public function getIdProvoz()
	{
		return $this->id_provoz;
	}

	/**
	 * @return int
	 */
	public function getMaxSendAttempts()
	{
		return $this->max_send_attempts;
	}

	/**
	 * @return boolean
	 */
	public function isOvereni()
	{
		return $this->overeni;
	}

	/**
	 * @return bool
	 */
	public function isNoEmailSend()
	{
		return $this->no_email_send;
	}

	/**
	 * @return boolean
	 */
	public function isProduction()
	{
		return $this->production;
	}

	/**
	 * @return boolean
	 */
	public function isDevelopment()
	{
		return !$this->isProduction();
	}

	/**
	 * @return string
	 */
	public function getDispatcher()
	{
		return $this->dispatcher;
	}

	/**
	 * @return boolean
	 */
	public function isRezim()
	{
		return $this->rezim;
	}

	/**
	 * @return string
	 */
	public function getPassword()
	{
		return $this->password;
	}

	/**
	 * @return string
	 */
	public function getApiProduction()
	{
		return $this->api_production;
	}

	/**
	 * @return string
	 */
	public function getApiDevelopment()
	{
		return $this->api_development;
	}

	/**
	 * @return string
	 */
	public function getCertDir()
	{
		return $this->certDir;
	}

	/**
	 * @return array
	 */
	public function getLanguages()
	{
		if (!is_array($this->languages)) {
			$lang = explode(',',
				Strings::lower(
					Strings::replace($this->languages, [
						'~\s+~' => '',
					])
				)
			);

			/**
			 * Remove empty item
			 */
			if (count($lang) === 1 && empty($lang[0])) {
				$lang = [];
			}

			/**
			 * Add default language if not present
			 */
			if (!in_array(self::LANGUAGE_DEFAULT, $lang)) {
				array_unshift($lang, self::LANGUAGE_DEFAULT);
			}

			$this->languages = $lang;
		}

		return $this->languages;
	}

	/**
	 * @todo Zbavit se global $engine ap.
	 *
	 * @param string    $status
	 */
	public function setCronStatus($status)
	{
		if (!array_key_exists($status, CronStatus::getList())) {
			throw new \Exception(sprintf('Unknown CRON status "%s"', $status));
		}

		bdump([
			__FUNCTION__,
			'current' => $this->getCronStatus(),
			'new' => $status,
		]);

		/**
		 * @Cron Status File
		 */
		file_put_contents($this->cronStatusFile, $status);
		chmod($this->cronStatusFileUnsafe, 0777);

		$this->cronStatus = $status;
	}

	/**
	 * @return string
	 */
	public function getCronStatus()
	{
		return $this->cronStatus;
	}

	/**
	 * @return array
	 */
	public function getTemplates()
	{
		return $this->templates;
	}

	/**
	 * @return array
	 */
	public static function getDispatcherList()
	{
		return [
			self::MF_DISPATCHER => 'Ministerstvo financí',
			self::WNI_DISPATCHER => 'Wincor Nixdorf',
		];
	}
}
