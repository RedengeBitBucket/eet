<?php

use Nette\Utils\Strings;
use Redenge\EET\Model\Transaction;

$price = (float) Strings::random(3, '0-9');
$dph = 21;

$transaction = (new Transaction())
	// ->setEmail()
	->setPoradCis(Strings::random(8))
	->setCelkTrzba($price)
	->setZaklDan1(\Redenge\EET\Price::getBasePrice($price, $dph))
	// ->setLanguage('en')
	->setDan1($dph);

// $service->save($transaction);
// $service->sendTransactions();

// bdump($transaction);
// $trans = $engine->shop->eet->get(115);
// bdump($trans);

$currency = 'EUR';
$rates = \Redenge\Money\CNB\ExchangeRateFactory::create();
try {
	$byService = TRUE;
	$euroRate = $rates->get($currency);
} catch (\Exception $e) {
	$byService = FALSE;
	// $euroRate = $engine->shop->currency->getExchangeRateByCode($currency);
}

bdump([
	'Kurz CZK/EUR' => $euroRate,
	'by_service' => $byService,
]);
