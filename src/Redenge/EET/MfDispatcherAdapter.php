<?php

namespace Redenge\EET;

use Ondrejnov\EET\Dispatcher;
use Ondrejnov\EET\Receipt;
use Redenge\EET\Model\Transaction;

class MfDispatcherAdapter implements IDispatcher
{
	const   API_PRODUCTION = 'https://prod.eet.cz:443/eet/services/EETServiceSOAP/v3?wsdl',
			API_DEVELOPMENT = 'https://pg.eet.cz:443/eet/services/EETServiceSOAP/v3?wsdl';

	/** @var Settings */
	private $settings;

	/** @var Dispatcher */
	private $dispatcher;

	/** @var string */
	private $key;

	/**
	 * @param Settings    $settings
	 */
	public function __construct(Settings $settings)
	{
		$this->settings = $settings;

		$apiProduction = $this->settings->getApiProduction() ?: self::API_PRODUCTION;
		$apiDevelopment = $this->settings->getApiDevelopment() ?: self::API_DEVELOPMENT;

		$wsdl = $this->settings->isProduction() ? $apiProduction : $apiDevelopment;
		$this->key = sprintf('%s/%s', $this->settings->getCertDir(), Certificate::KEY_NAME);
		$cert = sprintf('%s/%s', $this->settings->getCertDir(), Certificate::CERT_NAME);

		bdump([
			'wsdl' => $wsdl,
			'key' => file_exists($this->key),
			'cert' => file_exists($cert),
			'dir' => $this->settings->getCertDir(),
		]);

		$this->dispatcher = new Dispatcher($wsdl, $this->key, $cert);
		$this->dispatcher->trace = TRUE;
	}

	/**
	 * @param Transaction    $transaction
	 *
	 * @return Transaction
	 */
	public function send(Transaction $transaction)
	{
		try {
			$receipt = new Receipt();
			$receipt->uuid_zpravy = $transaction->getUuidZpravy();
			$receipt->prvni_zaslani = $transaction->getPrvniZaslani();
			$receipt->dic_popl = $transaction->getDicPopl();
			$receipt->dic_poverujiciho = $transaction->getDicPoverujiciho();
			$receipt->id_provoz = $transaction->getIdProvoz();
			$receipt->id_pokl = $transaction->getIdPokl();
			$receipt->porad_cis = $transaction->getPoradCis();
			$receipt->dat_trzby = $transaction->getDatTrzby(TRUE); // \DateTime
			$receipt->celk_trzba = $transaction->getCelkTrzba();
			$receipt->zakl_nepodl_dph = $transaction->getZaklNepodlDph();
			$receipt->zakl_dan1 = $transaction->getZaklDan1();
			$receipt->dan1 = $transaction->getDan1();
			$receipt->zakl_dan2 = $transaction->getZaklDan2();
			$receipt->dan2 = $transaction->getDan2();
			$receipt->zakl_dan3 = $transaction->getZaklDan3();
			$receipt->dan3 = $transaction->getDan3();
			$receipt->cest_sluz = $transaction->getCestSluz();
			$receipt->pouzit_zboz1 = $transaction->getPouzitZboz1();
			$receipt->pouzit_zboz2 = $transaction->getPouzitZboz2();
			$receipt->pouzit_zboz3 = $transaction->getPouzitZboz3();
			$receipt->urceno_cerp_zuct = $transaction->getUrcenoCerpZuct();
			$receipt->cerp_zuct = $transaction->getCerpZuct();
			$receipt->rezim = $transaction->getRezim();
			// $receipt->dat_odesl = $now; // Chybí v MF @todo

			// $receipt->email = $transaction->get(); 	// Nemění se, Done
			// $receipt->pkp = $transaction->get();		// Nemění se, Done
			// $receipt->bkp = $transaction->get();		// Nemění se, Done

			// $receipt->fik = $transaction->get();			// Vrací dispatcher // Done
			// $receipt->test = $transaction->get();		// Vrací dispatcher // Done
			// $receipt->dat_prij = $transaction->get();	// Vrací dispatcher // Done

			// $receipt->state = $transaction->get();		// Aktualizujeme po odeslání // Done 3/3
			// $receipt->counter = $transaction->get();		// Aktualizujeme po odeslání // Done
			// $receipt->notify_sent = $transaction->get(); // Aktualizujeme po odeslání // Done

			/* Example
			$response = [
				'Hlavicka' => [
					'uuid_zpravy' => '7d594159-2ace-4f0a-9442-ead641b05eb7'
					'bkp' => '2891737b-d1a7972e-4b9dac62-afdfbe98-4ae58b95',
					'dat_prij' => '2017-02-07T15:21:16+01:00',
				],
				'Potvrzeni' => [
					'fik' => '9ea542b3-98a4-4c74-8d6c-37e68da62fa7-ff',
					'test' => TRUE,
				],
			];
			*/
			$this->dispatcher->send($receipt, $transaction->getOvereni());
			$response = $this->dispatcher->getWholeResponse();

			bdump(['response t' => $response]);

			if (isset($response->Potvrzeni)) {
				$transaction->setState(State::SUCCESS);
				$transaction->setFik($response->Potvrzeni->fik);
				$transaction->setTest(isset($response->Potvrzeni->test) ? $response->Potvrzeni->test : FALSE);
				$transaction->setDatPrij($response->Hlavicka->dat_prij);

				bdump([
					'check' => $response->Hlavicka->bkp === $transaction->getBkp() && $response->Hlavicka->uuid_zpravy === $transaction->getUuidZpravy(),
					'bkp' => $response->Hlavicka->bkp === $transaction->getBkp(),
					'uuid' => $response->Hlavicka->uuid_zpravy === $transaction->getUuidZpravy(),
				]);
			}

			if ($transaction->getOvereni()) {
				throw new \Exception('Ověřovací mód odeslání je zapnutý. Použije se PKP a BKP.');
			}
		} catch (ServerException $e) {
			/* Example
			$response = [
				'Hlavicka' => [
					'uuid_zpravy' => 'aaf541f6-1fbe-44b3-996c-4003715ecfac',
					'dat_odmit' => '2017-02-07T14:05:44+01:00',
				],
				'Chyba' => [
					'kod' => 3,
					'test' => TRUE,
				]
			];
			*/
			// bdump($e);
			// $message = sprintf('Chyba: %s, kod = %s', $e->getMessage(), $e->getCode());
			$transaction->call->message = sprintf('Chyba: %s, kod = %s', $e->getMessage(), $e->getCode());
			$transaction->setState(State::ERROR);
			// $transaction->setTest($response->Chyba->test);
		} catch (\Exception $e) {
			// bdump($e);
			$transaction->call->message = sprintf('Chyba: %s, kod = %s', $e->getMessage(), $e->getCode());
			$transaction->setState(State::TIMEOUT);
			$transaction->setTest($this->settings->isDevelopment());
		}

		return $transaction;
	}

	/**
	 * Check codes (BKP and PKP) are generated depending on Transaction properties:
	 *  - dic_popl
	 *  - id_provoz
	 *  - id_pokl
	 *  - porad_cis
	 *  - dat_trzby
	 *  - celk_trzba
	 *
	 * @param Transaction $transaction
	 *
	 * @return array
	 */
	public function getCheckCodes(Transaction $transaction)
	{
		return CheckCodes::get($transaction, $this->key);
	}

	/**
	 * @return string
	 */
	public function getLastRequest()
	{
		return $this->dispatcher->getSoapClient()->__getLastRequest();
	}

	/**
	 * @return string
	 */
	public function getLastResponse()
	{
		return $this->dispatcher->getSoapClient()->__getLastResponse();
	}
}
