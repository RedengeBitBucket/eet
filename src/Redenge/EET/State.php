<?php

namespace Redenge\EET;

class State extends \Nette\Object
{
	const   CREATED = 'new',
			SUCCESS = 'success',
			ERROR = 'error',
			TIMEOUT = 'timeout',
			BILL = 'bill';

	/**
	 * @return array
	 */
	public static function getList()
	{
		return [
			self::CREATED => 'Nová',
			self::SUCCESS => 'Přijata',
			self::ERROR => 'Chyba',
			self::TIMEOUT => 'Timeout',
			self::BILL => 'Nová (Účtenka)',
		];
	}
}
