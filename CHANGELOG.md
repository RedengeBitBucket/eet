# Changelog

## 1.12.6
 - `Redenge\EET\Model\Transaction`
    - přidán chybějící parametr `filter` jako null


##1.12.0
 - `Redenge\EET\WniAdapter`
    - nasteveno výchozí časové pásmo Evropa/Praha pro `dat_trzby`
    - uložení  odchozích requestů ve formátu JSON do databáze `eet_transaction_call.request`
 - `Redenge\EET\Model\Transaction`
    - jako datum vystavení účtenky pro tisknutelnou fromu se propíše vždy `dat_trzby` i když je nastaven FIK  

## 1.11.0
- `Redenge\EET\Model\Transaction` rozšírena o atribut `currency` (default = 'CZK');
- Pokud transakci nastavíme měnu jinou než defaultní (CZK), tak dojde k přepočtu ceny do CZK podle kurzu ČNB
- **DŮLEŽITÉ - měnu musíme nastavit dříve než ceny (jinak se použije defaultní měna - CZK)**

## 1.10.0
- `Redenge\EET\Service::sendTransactions()` má nyni mechanizmus aby běžel maximálně jednou.
- Vyžaduje zápis do složky `temp` v **DocumentRoot**
- Využívá `nette/safe-stream`

## v1.9.0
- `Redenge\EET\Model\Transaction` rozšírena o atribut `languages` (default = 'cs');
- Nově lze účtenku kompletně přeložit pro jazyky uvedené v **EET » Nastavení » Jazyky**, by default `cs`, více jazyků oddělíme čárkou
- Pokud není vyplněný *překlad šablony* pro daný jazyk, použije se výchozí `cs`
- Pokud není vyplněný výchozí překlad, je použit statický
- Odebrán header a footer účtenky z nastavení - nyní jsou součásti *překladu šablony*
- Při přechodu z verze < 1.9.0 je potřeba upravit tabulku `eet_transaction`:

```sql
ALTER TABLE `eet_transaction`
ADD `language` varchar(2) COLLATE 'utf8_general_ci' NOT NULL AFTER `notify_sent`;
```

## v1.3.0
- Přidána instalace certifikátu .p12

## v0.1.0
- Relase první verze
