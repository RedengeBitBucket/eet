# EET

Modul obsahuje rozšíření pro administraci a poskytuje rozhraní pro _elektronickou evidenci tržeb_ pro **Ministerstvo Financi** a **Wincor Nixdorf**.

#Upgrade starší verze EET modulu:
 V připadě upgradu EET modulu z verze menší než  v1.12.6 je potřeba upravit třídu DBObject dle bodu 3 v návodu k instalaci

## Instalace

1) Závislosti nainstalujeme pomocí [Composer](https://getcomposer.org):

```bash
composer require redenge/eet
```
> V composer.json projektu je potřeba mít `"repositories": [{"type": "composer", "url": "https://satis.redenge.biz"}]`

2) V `admin/index.php` načteme modul pro administraci:

```php
// admin/index.php
$adminModuleLoader = new Redenge\Admin\ModuleLoader($engine, $_components, [
	'Redenge\Admin\Module\EET',
]);
```

3) Provedeme úpravy na shopu:
```php
// 1. Rozšíříme \DBObject settings (settings.php) o metodu
/**
 * @param string|int $theme
 */
public function getByTheme($theme, $themeColumnName = 'theme')
{
	$settings = [];
	$records = $this->getRecords('code, setting', NULL, NULL, sprintf('%s = %d', $themeColumnName, $theme));
	while ($row = mysql_fetch_assoc($records)) {
		$settings[$row['code']] = $row['setting'];
	}
	mysql_free_result($records);

	return $settings;
}

// 2. Přidáme nasleující metody do DBObject
/**
 * @return array
 */
public function getArray() {
	$properties = [];
	foreach ($this->vars as $name => $property) {
		if ($this->{$name} instanceof DBObject) {
			$properties[$name] = $this->{$name}->getArray();
		} else {
			$properties[$name] = $this->{$name};
		}
	}

	return $properties;
}

/**
 * @param array $values
 */
public function setValues(\Nette\Utils\ArrayHash $values) {
	foreach ($values as $name => $value) {
		$this->{$name} = $value;
	}
}
// 3. upravíme podle potřeby core\class\DBObject.php funkci getId()
public function getId($filters)
	{
		$sql = sprintf('SELECT id FROM %s', $this->table);

		if (is_array($filters)) {
			if (sizeof($filters) > 0) {
    	    	$sql = sprintf('%s WHERE %s', $sql, join($filters, ' AND '));
			}
		} else if ($filters != '') {
			$sql = sprintf('%s WHERE %s', $sql, $filters);
		}

 		$result = $this->db->getRecords($sql);
		$id = mysqli_fetch_array($result);

		return $id === null ? 0 : $id;
	}

// 4. Dle potřeby upravíme t_table
```

3) Rozšíříme databázi:

```sql
CREATE TABLE `eet_transaction` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`state` varchar(10) NOT NULL,
	`email` varchar(255) DEFAULT NULL,
	`uuid_zpravy` varchar(36) NOT NULL,
	`dat_odesl` datetime DEFAULT NULL,
	`prvni_zaslani` tinyint(1) NOT NULL,
	`overeni` tinyint(1) DEFAULT NULL,
	`dic_popl` varchar(12) NOT NULL,
	`dic_poverujiciho` varchar(12) DEFAULT NULL,
	`id_provoz` varchar(6) NOT NULL,
	`id_pokl` varchar(20) NOT NULL,
	`porad_cis` varchar(25) NOT NULL,
	`dat_trzby` datetime NOT NULL,
	`celk_trzba` decimal(8,2) NOT NULL,
	`zakl_nepodl_dph` decimal(8,2) DEFAULT NULL,
	`zakl_dan1` decimal(8,2) DEFAULT NULL,
	`dan1` decimal(8,2) DEFAULT NULL,
	`zakl_dan2` decimal(8,2) DEFAULT NULL,
	`dan2` decimal(8,2) DEFAULT NULL,
	`zakl_dan3` decimal(8,2) DEFAULT NULL,
	`dan3` decimal(8,2) DEFAULT NULL,
	`cest_sluz` decimal(8,2) DEFAULT NULL,
	`pouzit_zboz1` decimal(8,2) DEFAULT NULL,
	`pouzit_zboz2` decimal(8,2) DEFAULT NULL,
	`pouzit_zboz3` decimal(8,2) DEFAULT NULL,
	`urceno_cerp_zuct` decimal(8,2) DEFAULT NULL,
	`cerp_zuct` decimal(8,2) DEFAULT NULL,
	`rezim` tinyint(1) NOT NULL,
	`pkp` varchar(344) CHARACTER SET utf8 COLLATE utf8_bin DEFAULT NULL,
	`bkp` varchar(44) DEFAULT NULL,
	`dat_prij` datetime DEFAULT NULL,
	`fik` varchar(39) DEFAULT NULL,
	`test` tinyint(1) DEFAULT NULL,
	`counter` tinyint(2) NOT NULL,
	`notify_sent` int(11) NOT NULL,
	`language` varchar(2) NOT NULL,
	`currency` char(3) NOT NULL,
	`id_order` int(11) DEFAULT NULL COMMENT 'ID objednávky',
	`id_bill` int(11) DEFAULT NULL COMMENT 'ID dokladu',
	PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE `eet_transaction_call` (
	`id` int(11) NOT NULL AUTO_INCREMENT,
	`id_eet_transaction` int(11) NOT NULL,
	`created` datetime NOT NULL,
	`state` varchar(10) NOT NULL,
	`request` text NOT NULL,
	`response` text,
	`warning` tinyint(1) NOT NULL,
	`message` varchar(1024) NOT NULL,
	PRIMARY KEY (`id`),
	KEY `id_eet_transaction` (`id_eet_transaction`),
	CONSTRAINT `eet_transaction_call_ibfk_1` FOREIGN KEY (`id_eet_transaction`) REFERENCES `eet_transaction` (`id`) ON DELETE NO ACTION,
	CONSTRAINT `eet_transaction_call_ibfk_2` FOREIGN KEY (`id_eet_transaction`) REFERENCES `eet_transaction` (`id`) ON DELETE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
```

4) A povolíme zápis do složky `temp` v **DocumentRoot**



## Použití
Vytvoření služby
```php
$service = new \Redenge\EET\Service($engine->shop->eet, new \Redenge\EET\Settings($engine->shop->settings->getByTheme(\Redenge\Admin\Module\EET::SETTINGS_THEME)));
```

Případně přes továrničku
```php
/** @var \ShopModel $shop */
$service = \Redenge\EET\ServiceFactory::create($shop);
```

Přiání nové transakce
```php
$dph = 21;
$price = 123.45;
$transaction = (new \Redenge\EET\Model\Transaction())
	->setEmail('JohnDoe@example.com')
	->setCurrency('CZK')
	->setPoradCis('ASD0000001')
	->setCelkTrzba($price)
	->setZaklDan1(\Redenge\EET\Price::getBasePrice($price, $dph))
	->setDan1(\Redenge\EET\Price::getTaxPrice($price, $dph);

$service->save($transaction);
```

Odeslání transakce (např. cronem)
```php
$service->send();
```

## Požadavky

- PHP: >= 5.6
- redenge/admin: ~1.0
- ondrejnov/eet: @dev
- nette/utils: ~2.3
- nette/forms: ~2.3
- swiftmailer/swiftmailer: ^5.4
- mpdf/mpdf: ^6.1
- guzzlehttp/guzzle: ~6.2
- erusev/parsedown: ^1.6
- redenge/base 1.1.0 (od v1.12.6)