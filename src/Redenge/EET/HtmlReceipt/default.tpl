<!-- Výchozí template -->
<div class="uctenka" style="margin: 0; padding: 0; font-family: arial; font-size: 12px;">
	<header>
		<h4 style="text-align: center;"><strong>EET Účtenka</strong></h4>
		<hr />
	</header>
	<div>
		<table>
			<tr>
				<td style="text-align: right; font-weight: bold; width: 50%">Celková cena</td><td style="text-align: left">{CELK_TRZBA} Kč</td>
			</tr>
		</table>
	</div>
	<hr />
	<div sytle="text-align: left">
		<dl>
			<dt>DIČ poplatníka</dt><dd>{DIC_POPL}</dd>
			<dt>Režim tržby</dt><dd>{REZIM}</dd>
			<dt>Provozovna</dt><dd>{ID_PROVOZ}</dd>
			<dt>Pokladna</dt><dd>{ID_POKL}</dd>
			<dt>Číslo účtenky</dt><dd>{PORAD_CIS}</dd>
			<dt>Datum</dt><dd>{DATUM}</dd>
			<dt>BKP</dt><dd>{BKP}</dd>
			<dt>{SECONDARY_CODE_NAME}</dt><dd>{SECONDARY_CODE}</dd>
		</dl>
	</div>
	<footer>
		<hr />
		<p style="text-align: center;">~ Děkujeme za Váš nákup ~</p>
	</footer>
</div>