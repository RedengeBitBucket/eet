<?php

namespace Redenge\EET;

class Price
{
	/**
	 * @param float|int    $price
	 * @param float|int    $tax
	 *
	 * @return float|int
	 */
	public static function getBasePrice($price, $tax)
	{
		return $price / (1 + $tax / 100);
	}

	/**
	 * @param float|int    $price
	 * @param float|int    $tax
	 *
	 * @return float|int
	 */
	public static function getTaxPrice($price, $tax)
	{
		return self::getBasePrice($price, $tax) * ($tax / 100);
	}
}
