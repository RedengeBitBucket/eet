<?php

namespace Redenge\EET;

class CronStatus
{
	const SETTINGS_CODE = 'eet_cron_status';

	const   READY = 'ready',
			ERROR = 'error',
			RUNNING = 'running';

	/**
	 * @param bool    $prompt
	 *
	 * @return array
	 */
	public static function getList($prompt = FALSE)
	{
		$list = [
			self::READY => 'Připraven',
			self::ERROR => 'Chyba',
			self::RUNNING => 'Probíhá',
		];

		array_unshift($list, 'NA');

		return $list;
	}
}
