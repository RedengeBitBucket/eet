<div id="global-menu">
	<!-- START BLOCK : menu -->
	<div id="icons_menu">
		<a href="{LINK}" class="link16 l_new2" title="{NAME}">{NAME}</a>
	</div>
	<!-- END BLOCK : menu -->
</div>
{FILTER}
{TABLE}
<div class="selected_action">
	<div class="selected_action_in">
		<!-- START BLOCK : select-action -->
		<span class="sel_nav">
			<img src="images/arrow_ltr.gif" align="absmiddle"/>
			<a href="#" onClick="select_all(document.forms['start_form'].elements['{DROP_PREFIX}drop[]']);return false;">{T_SELECT_ALL}</a> /
			<a href="#" onClick="unselect_all(document.forms['start_form'].elements['{DROP_PREFIX}drop[]']);return false;">{T_UNSELECT_ALL}</a>
			&nbsp; <strong><i>{T_CHECKED}</i></strong>:
			<!-- START BLOCK : drop_button -->
			<input type="submit" value="{T_DELETEDELETE}" class="image" name="{DROP_PREFIX}drop_select" onClick="return my_alert('{T_SELECTED}');"/>
			<!-- END BLOCK : drop_button -->
		</span>
		<br />
		<br />
		<!-- END BLOCK : select-action -->
		{NAVIGATOR}
	</div>
</div>
