<?php

use Nette\Forms\Form;
use Nette\Utils\Validators;

$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', basename(__FILE__, '.php')));
$template->prepare();

/**
 * Výchozí hodnoty
 */
if ($engine->shop->eet->id) {
	$defaults = $engine->shop->eet->getArray();
	if ($isStorno) {
		$defaults = $engine->shop->eet->getStornoValues($defaults);
	}
} else {
	$defaults = [];
}

$notifySent = isset($defaults['notify_sent']) ? $defaults['notify_sent'] : FALSE;
$customerEmail = Validators::isEmail($defaults['email']) && !empty($defaults['pkp']) && !empty($defaults['bkp']) ? $defaults['email'] : FALSE;

$form = new Form;
$form->addSelect('state', 'Stav', \Redenge\EET\State::getList())
    ->setRequired();
$form->addText('email', 'Email')
    ->addCondition(Form::FILLED)
    ->addRule(Form::EMAIL);
$form->addText('uuid_zpravy', 'UUID zprávy')
    ->setRequired();
$form->addText('dat_odesl', 'Datum a čas odeslání zprávy');
$form->addCheckbox('prvni_zaslani', 'První zaslání údajů o tržbě');
$form->addCheckbox('overeni', 'Ověřovací mód odeslání'); // Příznak ověřovacího módu odeslání
$form->addText('dic_popl', 'DIČ poplatníka')
    ->setRequired();
$form->addText('dic_poverujiciho', 'DIČ pověřujícího poplatníka');
$form->addText('id_provoz', 'Označení provozovny')
    ->setRequired();
$form->addText('id_pokl', 'Označení pokladního zařízení')
    ->setRequired();
$form->addText('porad_cis', 'Pořadové číslo účtenky')
    ->setRequired();
$form->addText('dat_trzby', 'Datum a čas přijetí tržby')
    ->setRequired();
$form->addText('celk_trzba', 'Celková částka tržby')
    ->setRequired()
    ->addRule(Form::FLOAT);
$form->addText('zakl_nepodl_dph', 'Celková částka plnění osvobozených od DPH, ostatních plnění')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('zakl_dan1', 'Celkový základ daně se základní sazbou DPH')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('dan1', 'Celková DPH se základní sazbou')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('zakl_dan2', 'Celkový základ daně s první sníženou sazbou DPH')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('dan2', 'Celková DPH s první sníženou sazbou')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('zakl_dan3', 'Celkový základ daně s druhou sníženou sazbou DPH')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('dan3', 'Celková DPH s druhou sníženou sazbou')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('cest_sluz', 'Celková částka v režimu DPH pro cestovní službu')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('pouzit_zboz1', 'Celková částka v režimu DPH pro prodej použitého zboží se základní sazbou')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('pouzit_zboz2', 'Celková částka v režimu DPH pro prodej použitého zboží s první sníženou sazbou')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('pouzit_zboz3', 'Celková částka v režimu DPH pro prodej použitého zboží s druhou sníženou sazbou')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('urceno_cerp_zuct', 'Celková částka plateb určená k následnému čerpání nebo zúčtování')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addText('cerp_zuct', 'Celková částka plateb, které jsou následným čerpáním nebo zúčtováním platby')
    ->addRule(Form::FLOAT)
    ->setRequired(false);
$form->addCheckbox('rezim', _('Zjednodušený režim')) // Režim tržby [0 = bezny, 1 => zjednoduseny]
->setDisabled();
$form->addText('pkp', 'Podpisový kód poplatníka (PKP)');
$form->addText('bkp', 'Bezpečnostní kód poplatníka (BKP)');
$form->addText('dat_prij', 'Datum a čas přijetí zprávy');
$form->addText('fik', 'Fiskální identifikační kód (FIK)');
$form->addCheckbox('test', 'Testovací prostředí'); // Příznak neprodukčního prostředí
$form->addText('counter', 'Počítadlo');
$form->addCheckbox('notify_sent', 'Odeslaná účtenka zákazníkovi' . ($notifySent ? sprintf(' (%d×)', $notifySent) : '')); // Počet odeslání účtenky zákazníkovi
$form->addText('id_order', 'Číslo objednávky');
$form->addText('id_bill', 'Číslo dokladu');
$form->addText('language', 'Jazyk');

$form->addSubmit('send', _('Uložit'));

/**
 * Výchozí hodnoty
 */
$form->setDefaults($defaults);

/**
 * Odeslání formuláře
 * @todo Předělat na Transaction
 */
if ($form->isSuccess()) {
	$values = $form->getValues();

	if ($isStorno || $engine->shop->eet->id === 0) {
		$engine->shop->eet->reset();
	}

	$engine->shop->eet->setValues($values);
	if (!$engine->shop->eet->save()) {
		throw new \Exception('Problem when saving EET transaction.');
	}

	if ($isStorno) {
		$engine->redirect(htmlspecialchars_decode(sprintf('%s&amp;page=%d', $_internal_link, $_page)));
	}
}

$htmlReceipt = $service->getHtmlReceipt($engine->shop->eet->get($engine->shop->eet->id));
if ($htmlReceipt && !$isStorno) {
	$receipt = $htmlReceipt;
	$pdfLink = sprintf('<a href="%s" title="%s"><img src="/admin/images/%s" alt=""></a>', sprintf('%s&amp;page=%d&amp;edit=%d&amp;receipt', $_internal_link, $_page, $engine->shop->eet->id), 'PDF', 'ordernote.png');
	$notifyLink = $customerEmail ? sprintf('<a href="%s" title="%s"><img src="/admin/images/%s" alt=""></a>', sprintf('%s&amp;page=%d&amp;edit=%d&amp;notify', $_internal_link, $_page, $engine->shop->eet->id), 'Send', 'email.png') : '';
} else {
	$receipt = $isStorno ? '' : 'EET Účtenku nelze zobrazit.';
	$pdfLink = '';
	$notifyLink = '';
}

$template->assign('FORM', (string) $form);
$template->assign('RECEIPT', (string) $receipt);
$template->assign('PDF_LINK', (string) $pdfLink);
$template->assign('NOTIFY_LINK', (string) $notifyLink);

$templateLevel2->assign('BODY', $template->getOutputContent());
