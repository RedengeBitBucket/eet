<?php

namespace Redenge\EET;

use Ondrejnov\EET\Utils\Format;
use Redenge\EET\Model\Transaction;
use RobRichards\XMLSecLibs\XMLSecurityKey;

class CheckCodes
{
	/**
	 * @param Transaction    $transaction
     * @param string         $key
	 *
	 * @return array
	 */
	public static function get(Transaction $transaction, $key)
	{
		$codes = self::calculate($transaction, $key);

		return [
			'bkp' => $codes['bkp']['_'],
			'pkp' => base64_encode($codes['pkp']['_']),
		];
	}

	/**
	 * @param Transaction    $transaction
     * @param string         $key
	 *
	 * @return array
	 */
	public static function calculate(Transaction $transaction, $key)
	{
		$objKey = new XMLSecurityKey(XMLSecurityKey::RSA_SHA256, ['type' => 'private']);
        $objKey->loadKey($key, TRUE);

        $arr = [
            $transaction->getDicPopl(),
            $transaction->getIdProvoz(),
            $transaction->getIdPokl(),
            $transaction->getPoradCis(),
            $transaction->getDatTrzby(TRUE)->format('c'),
            $transaction->getCelkTrzba(),
        ];
        $sign = $objKey->signData(join('|', $arr));

        return [
            'pkp' => [
                '_' => $sign,
                'digest' => 'SHA256',
                'cipher' => 'RSA2048',
                'encoding' => 'base64'
            ],
            'bkp' => [
                '_' => Format::BKP(sha1($sign)),
                'digest' => 'SHA1',
                'encoding' => 'base16'
            ]
        ];
	}
}
