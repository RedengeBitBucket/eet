<?php

/**
 * Transaction detail
 */
if (isset($_GET['edit'])) {

	$engine->shop->eet->load_by([
		'id' => (int) $_GET['edit'],
	]);

	/**
	 * Tisk PDF účtenky
	 */
	if (isset($_GET['receipt'])) {
		bdump($service->getPdfReceipt($engine->shop->eet->get($engine->shop->eet->id), $download = TRUE));
		return;
	}

	/**
	 * Odeslání emailové notifikace
	 */
	if ($sendNotify = isset($_GET['notify']) && $engine->shop->eet->id) {
		bdump(['id' => $engine->shop->eet->id]);
		$transaction = $engine->shop->eet->get($engine->shop->eet->id);
		if ($service->sendEmailNotification($transaction)) {
			$transaction->setNotifySent($transaction->getNotifySent() + 1);
			$engine->shop->eet->persist($transaction);
		}
		$engine->redirect(htmlspecialchars_decode(sprintf('%s&amp;page=%d&amp;edit=%d', $_internal_link, $_page, $engine->shop->eet->id)));
		return;
	}

	$templateLevel2 = new \TemplatePower(ENGINE_PATH . '/lib/templates/start_in.tpl');
	$templateLevel2->prepare();

	$tabsLevel2 = [
		'transactionDetail' => _('Detail'),
		'transactionRequests' => _('Volání'),
	];

	$isStorno = isset($_GET['storno']) && $engine->shop->eet->id;

	$_submenu = ($_submenu != '') ? $_submenu : 'transactionDetail';

	$_special_parametr .= sprintf("&amp;edit=%d&amp;submenu=%s%s", $engine->shop->eet->id, $_submenu, $isStorno ? '&amp;storno' : '');

	array_push($_location, sprintf("%s: %s", _('Detail'), $engine->shop->order_interface->order->{$orderIndexColumnName}));

	if (array_key_exists($_submenu, $tabsLevel2)) {
		array_push($_location, $tabsLevel2[$_submenu]);
	}

	$file = sprintf('%s/%s.php', __DIR__, $_submenu);
	if (!file_exists($file)) {
		throw new Exception("FATAL ERROR! Warning: Cannot find $file");
	}

	include($file);

	$templateLevel2->assign('ZALOZKY', _get_bookmarks(sprintf("%s&amp;edit=%d&amp;page=%d%s", $_internal_link, $engine->shop->eet->id, $_page, $isStorno ? '&amp;storno' : ''), $tabsLevel2, $_submenu, TRUE));
	$templateLevel1->assign('BODY', $templateLevel2->getOutputContent());

	return;
}

/**
 * Send transactions
 */
if (isset($_GET['sendTransactions'])) {
	$service->sendTransactions();
	$engine->redirect($engine->redirect(htmlspecialchars_decode(sprintf('%s&amp;page=%d', $_internal_link, $_page))));
}

/**
 * Transactions list
 */
$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', 'simpleList'));
$template->prepare();

$template->newBlock('menu');
$template->assign('LINK', sprintf('%s&amp;sendTransactions', $_internal_link));
$template->assign('NAME', _('Odeslat transakce'));
$template->gotoBlock('_ROOT');

$template->assign('T_CHECKED', _('Zaškrtnuté'));
$template->assign('T_SELECT_ALL', _('Označit vše'));
$template->assign('T_UNSELECT_ALL', _('Odznačit vše'));

$args = [];
$filters = [];
$table = new t_table('eet_transaction', $engine->admin, _edit_right(), $_component_name);

// $table->create_standardfields();
$table->create_text_field('id', _('ID'));
$table->create_text_field('detail', '');
$table->create_text_field('storno', _('Storno'));
$table->create_text_field('receipt', _('Účtenka'));
$table->create_text_field('id_order', _('Číslo objednávky'));
$table->create_text_field('id_bill', _('Čislo dokladu'));
$table->create_text_field('porad_cis', _('Pořadové číslo'));
$table->create_text_field('state', _('Stav'));
$table->create_text_field('email', _('Email'));
$table->create_text_field('celk_trzba', _('Tržba'));
$table->create_text_field('fik', _('FIK'));
$table->create_date_field('dat_prij', _('Datum přijetí'));
$table->create_date_field('dat_trzby', _('Datum tržby'));
$table->create_date_field('etc_created', _('Datum odeslání'));
$table->create_text_field('counter', _('Počet odeslání'));
$table->create_text_field('notify_sent', _('Odeslaná účtenka'));
$table->create_bool_field('etc_warning', _('Varování'));
$table->create_bool_field('test', _('Test'));

$table->set_sort_fields('id', 'id_order', 'id_bill', 'porad_cis', 'state', 'email', 'celk_trzba', 'fik', 'dat_prij', 'dat_trzby', 'etc_created', 'counter', 'notify_sent', 'etc_warning', 'test');
$table->set_variable_field('id', FALSE);
$table->set_variable_field('id_order', FALSE);
$table->set_variable_field('id_bill', FALSE);
$table->set_variable_field('counter', FALSE);
$table->set_variable_field('etc_warning', FALSE);
$table->set_variable_field('test', FALSE);
$table->set_variable_field('notify_sent', FALSE);
$table->set_variable_field('dat_prij', FALSE);
$table->set_variable_field('dat_trzby', FALSE);
$table->set_variable_field('etc_created', FALSE);

// @Compatibility Fix
$register_searchbox_text_equal_name = method_exists($table, 'register_searchbox_text_equal') ? 'register_searchbox_text_equal' : 'register_searchbox_text';

$table->register_searchbox_text($filters, 'id', '', $args);
$table->register_searchbox_text($filters, 'id_order', '', $args);
$table->register_searchbox_text($filters, 'id_bill', '', $args);
$table->register_searchbox_text($filters, 'porad_cis', '', $args);
$table->register_searchbox_text($filters, 'state', 'eet_transaction.state', $args);
$table->register_searchbox_text($filters, 'email', '', $args);
$table->{$register_searchbox_text_equal_name}($filters, 'celk_trzba', '', $args);
$table->{$register_searchbox_text_equal_name}($filters, 'fik', '', $args);
$table->{$register_searchbox_text_equal_name}($filters, 'counter', '', $args);
$table->register_searchbox_bool($filters, 'notify_sent', '', $args);
$table->register_searchbox_bool($filters, 'test', '', $args);
$table->register_searchbox_bool($filters, 'etc_warning', 'eet_transaction_call.warning', $args);
$table->register_searchbox_date($filters, 'dat_prij', 'DATE(dat_prij)', $args);
$table->register_searchbox_date($filters, 'dat_trzby', 'DATE(dat_trzby)', $args);
$table->register_searchbox_date($filters, 'etc_created', 'DATE(eet_transaction_call.created)', $args);

$table->add_header(sprintf('%s&amp;page=%d', $_internal_link, $_page));

$join = 'LEFT JOIN eet_transaction_call ON eet_transaction_call.id_eet_transaction = eet_transaction.id';

$records = $engine->shop->eet->getPagedRecords($_page, $_limit, $_total, 'eet_transaction.*, MAX(eet_transaction_call.created) AS etc_created, eet_transaction_call.warning AS etc_warning', $join, $table->getSort(), implode(' AND ', $filters), 'eet_transaction.id', NULL, $args);
while ($row = mysqlc_fetch_assoc($records)) {
	$table->id = htmlspecialchars($row['id']);
	$table->detail = sprintf('<a href="%s" title="%s"><img src="/admin/images/%s" alt=""></a>', sprintf('%s&amp;page=%d&amp;edit=%d', $_internal_link, $_page, $row['id']), 'Detail', 'b_edit.png');

	$table->id_order = htmlspecialchars($row['id_order']);
	$table->id_bill = htmlspecialchars($row['id_bill']);
	$table->porad_cis = htmlspecialchars($row['porad_cis'] . ($row['id_bill'] ? ' (B)' : ''));
	$table->state = htmlspecialchars($row['state']);
	$table->email = htmlspecialchars($row['email'] ?: '-');
	$table->celk_trzba = htmlspecialchars($row['celk_trzba']);
	$table->fik = htmlspecialchars($row['fik'] ?: '-');
	$table->dat_prij = htmlspecialchars($row['dat_prij']);
	$table->dat_trzby = htmlspecialchars($row['dat_trzby']);
	$table->etc_created = htmlspecialchars($row['etc_created']);
	$table->counter = htmlspecialchars($row['counter']);
	$table->notify_sent = $row['notify_sent'] ? ('Ano' . ($row['notify_sent'] > 1 ? sprintf(' %d&times;', $row['notify_sent']) : '')) : 'Ne';
	$table->test = htmlspecialchars($row['test']);
	$table->etc_warning = $row['etc_warning'];

	if (!($row['state'] === \Redenge\EET\State::CREATED || $row['state'] === \Redenge\EET\State::BILL)) {
		$table->storno = sprintf('<a href="%s" title="%s"><img src="/admin/images/%s" alt=""></a>', sprintf('%s&amp;page=%d&amp;edit=%d&amp;storno', $_internal_link, $_page, $row['id']), 'Storno', 'back.png');
		$table->receipt = sprintf('<a href="%s" title="%s"><img src="/admin/images/%s" alt=""></a>', sprintf('%s&amp;page=%d&amp;edit=%d&amp;receipt', $_internal_link, $_page, $row['id']), 'Účtenka', 'ordernote.png');
	} else {
		$table->storno = '';
		$table->receipt = '';
	}

	if ($row['state'] === \Redenge\EET\State::ERROR || $row['dat_prij'] === '0000-00-00 00:00:00') {
		$color = 'ff0048'; // ERROR
	} elseif ($row['fik'] && $row['dat_prij'] !== '0000-00-00 00:00:00') {
		$color = 'c7ff00'; // OK
	}

	$table->add_line(isset($color) ? $color : NULL);
}

mysqlc_free_result($records);
$table->add_to_template($template);

_add_pager($template, $_page, $_total, $_limit, $_internal_link, $_strankovani_pole);

if (_edit_right()) {
	$template->newBlock('drop_button');
	$template->assign('T_SELECTED', _('označené'));
	$template->assign('T_DELETEDELETE', _('Smazat'));
}

$templateLevel1->assign('BODY', $template->getOutputContent());
