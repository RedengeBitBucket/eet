<?php

namespace Redenge\EET;

use Redenge\Admin\Module\EET;

class ServiceFactory implements IServiceFactory
{
	/**
	 * @param \ShopModel    $shop
	 *
	 * @return Service
	 */
	public static function create(\ShopModel $shop)
	{
		return new Service($shop->eet, new Settings($shop->settings->getByTheme(EET::SETTINGS_THEME)));
	}
}


interface IServiceFactory
{
	/**
	 * @param \ShopModel    $shop
	 *
	 * @return Service
	 */
	static function create(\ShopModel $shop);
}
