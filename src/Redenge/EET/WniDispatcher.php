<?php

namespace Redenge\EET;

use GuzzleHttp\Client;
use GuzzleHttp\Exception;
use Nette\Utils\Json;
use Ondrejnov\EET\Receipt;
use Ondrejnov\EET\Utils\Format;
use Redenge\EET\Model\Transaction;
date_default_timezone_set("Europe/Prague");

class WniDispatcher implements IDispatcher
{
	const METHOD = 'POST';

	/** @var Settings */
	private $settings;

	/** @var \GuzzleHttp\Client */
	private $client;

	/** @var array */
	private $cert;

	/** @var string */
	private $key;

	/** @var string */
	private $request;

	/** @var string */
	private $response;

	/**
	 * @param Settings    $settings
	 */
	public function __construct(Settings $settings)
	{
		$this->settings = $settings;
		$this->client = new Client();
		$this->api = $this->settings->isProduction() ? $this->settings->getApiProduction() : $this->settings->getApiDevelopment();
		$this->cert = sprintf('%s/%s', $this->settings->getCertDir(), $this->settings->getCert());
		$this->key = sprintf('%s/%s', $this->settings->getCertDir(), Certificate::KEY_NAME);
		bdump([
			'cert' => $this->cert,
			'key_' => $this->key,
		]);
	}

	/**
	 * @param Transaction    $transaction
	 *
	 * @return Transaction
	 */
	public function send(Transaction $transaction)
	{
		if (!file_exists($this->cert)) {
			throw new \Exception(sprintf('Certificate not found, path = %s', $this->cert));
		}

		try {
			$transaction = $this->sendRequest($transaction);
		} catch (Exception\ConnectException $e) {
			$transaction->call->message = sprintf('Chyba: %s, kod = %s', $e->getMessage(), $e->getCode());
			$transaction->setState(State::TIMEOUT);
		} catch (Exception\RequestException $e) {
			$transaction->call->message = sprintf('Chyba: %s, kod = %s', $e->getMessage(), $e->getCode());
			$transaction->setState(State::ERROR);
		} catch (Exception\ClientException $e) {
			$transaction->call->message = sprintf('Chyba: %s, kod = %s', $e->getMessage(), $e->getCode());
			$transaction->setState(State::ERROR);
		} catch (\Exception $e) {
			$transaction->call->message = sprintf('Chyba: %s, kod = %s', $e->getMessage(), $e->getCode());
			$transaction->setState(State::ERROR);
		}

		$transaction->setTest($this->settings->isDevelopment());

		return $transaction;
	}

	/**
	 * @Wincor Nixdorf API should return BKP and PKP codes, we can't create them due certs
	 *
	 * @param Transaction    $transaction
	 *
	 * @return array
	 */
	public function getCheckCodes(Transaction $transaction)
	{
		return FALSE;
	}

	/**
	 * @return string
	 */
	public function getLastRequest()
	{
		return $this->request;
	}

	/**
	 * @return string
	 */
	public function getLastResponse()
	{
		return $this->response;
	}

	/**
	 * @param Transaction    $transaction
	 *
	 * @return Transaction
	 * @throw \Excepiton
	 */
	private function sendRequest(Transaction $transaction)
	{
		$body = [
			'uuid' => $transaction->getUuidZpravy(),
			'repeated' => $transaction->getPrvniZaslani(),
			'vatId' => $transaction->getDicPopl(),
			'appointingVatId' => $transaction->getDicPoverujiciho(),
			'shopId' => $transaction->getIdProvoz(),
			'cashRegisterId' => $transaction->getIdPokl(),
			'receiptId' => $transaction->getPoradCis(),
            'receiptDate' => $this->formatReceiptDate((new \DateTime($transaction->getDatTrzby(false), new \DateTimeZone("Europe/Prague")))->format("c")),
			'total' => $transaction->getCelkTrzba(),
			'base0' => $transaction->getZaklNepodlDph(),
			'base1' => $transaction->getZaklDan1(),
			'tax1' => $transaction->getDan1(),
			'base2' => $transaction->getZaklDan2(),
			'tax2' => $transaction->getDan2(),
			'base3' => $transaction->getZaklDan3(),
			'tax3' => $transaction->getDan3(),
			'travelTotal' => $transaction->getCestSluz(),
			'used1Total' => $transaction->getPouzitZboz1(),
			'used2Total' => $transaction->getPouzitZboz2(),
			'used3Total' => $transaction->getPouzitZboz3(),
			'accNextTotal' => $transaction->getUrcenoCerpZuct(),
			'accTotal' => $transaction->getCerpZuct(),
			'saleMode' => $transaction->getRezim(),
		];

		$options = [
			'cert' => [$this->cert, $this->settings->getPassword()],
			'verify' => FALSE,
			'json' => $body,
		];

		// $request = $this->client->createRequest(self::METHOD, $this->api, $options);
		// bdump(['q' => $request->getQuery()]);

		// $response = $this->client->send($request);

        $this->request = json_encode($body);
		$response = $this->client->request(self::METHOD, $this->api, $options);
		$this->response = 'todo'; // @todo

		if ($response->getStatusCode() === 400) {
			throw new \Exception(sprintf('%d - Bad Request', $response->getStatusCode()));
		} elseif ($response->getStatusCode() === 404) {
			throw new \Exception(sprintf('%d - Not Found', $response->getStatusCode()));
		} elseif ($response->getStatusCode() !== 200) {
			throw new \Exception(sprintf('%s Error, HTTP Status Code = %d', get_class($this), $response->getStatusCode()));
		}

		$body = (object) Json::decode((string) $response->getBody());

		bdump([
			'status_code' => $response->getStatusCode(),
			'content_type' => $response->getHeaderLine('content-type'),
			'body_array' => $body,
			'headers' => $response->getHeaders(),
			'---',
			'check' => $body->pkp === $transaction->getPkp() && $body->bkp === $transaction->getBkp() && $body->uuid === $transaction->getUuidZpravy(),
			'pkp' => $body->pkp === $transaction->getPkp(),
			'bkp' => $body->bkp === $transaction->getBkp(),
			'uuid' => $body->uuid === $transaction->getUuidZpravy(),
			'pkp_' => $transaction->getPkp(),
			'bkp_' => $transaction->getBkp(),
		]);

		$this->response = Json::encode((array) $body);

		$transaction->setState(State::SUCCESS);
		$transaction->setFik($body->fik);
		$transaction->setDatPrij((new \DateTime($body->processedDate))->format('Y-m-d H:i:s'));
		$transaction->setPkp($body->pkp); // Aktualizujeme
		$transaction->setBkp($body->bkp); // Aktualizujeme

		if ($transaction->getOvereni()) {
			throw new \Exception('Ověřovací mód odeslání je zapnutý. Použije se PKP a BKP.');
		}

		return $transaction;
	}

    private function formatReceiptDate($date) {
        date_default_timezone_set("Europe/Prague");
        $date = explode("+",$date);
        $year = (date('Y'));
        $lastSundayOfMarch = date('m-d',strtotime("last sunday of March " . $year));
        $lastSundayOfOctober = date('m-d', strtotime("last sunday of October ". $year));
        $today = date('m-d', strtotime('now'));
        if($today < $lastSundayOfOctober && !($today < $lastSundayOfMarch)) {
            return $date[0] . "+02:00";
        } else {
            return $date[0] . "+01:00";
        }
    }
}
