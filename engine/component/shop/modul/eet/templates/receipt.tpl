<style type="text/css">
	.variabless {

	}
	.variables td {
		border: 1px solid black;
		padding: 5px;
	}
</style>
<table>
	<tr>
		<td>{FORM}</td>
		<td style="width: 50%; text-align: center;" valign="top">
			<h4>Dostupné proměnné</h4>
			<table class="variables">
				<!-- START BLOCK : variables -->
				<tr>
					<td>{NAME}</td>
					<td>&lbrace;{VARIABLE}&rbrace;</td>
				</tr>
				<!-- END BLOCK : variables -->
			</table>
		</td>
	</tr>
</table>