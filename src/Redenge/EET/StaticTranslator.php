<?php

namespace Redenge\EET;

use Nette\Localization\ITranslator;

class StaticTranslator
{
	/** @var array */
	private $transaltions = [
		'email.subject' => [
			'cs' => 'Vaše EET Účtenka',
			'en' => 'Your EET Receipt',
			'de' => 'Ihre EET Rechnung',
		],
		'email.body' => [
			'cs' => "Dobrý den,\n\nv příloze Vám zasíláme účtenku za Váš nákup.",
			'en' => "Dear customer,\n\nEET receipt for your purchase is in the attachment.",
			'de' => "Hallo, anbei erhält eine Rechnung für Ihren Kauf.",
		],
	];

	/** @var string */
	private $language;

	/** @var string */
	private $defaultLanguage = Settings::LANGUAGE_DEFAULT;

	/**
	 * @param string    $language
	 */
	public function __construct($language)
	{
		$this->language = $language;
	}

	/**
	 * @param  string   message
	 * @param  int      plural count
	 * @return string
	 */
	public function translate($message, $count = NULL)
	{
		if (!isset($this->transaltions[$message])) {
			throw new \Exception(sprintf('Translation "%s" not found', $message));
		}

		if (!isset($this->transaltions[$message][$this->language])) {
			trigger_error(sprintf('Transalte for "%s" in "%s" not found, using default language (%s)', $message, $this->language, $this->defaultLanguage));
			$lang = $this->defaultLanguage;
		} else {
			$lang = $this->language;
		}

		return $this->transaltions[$message][$lang];
	}
}
