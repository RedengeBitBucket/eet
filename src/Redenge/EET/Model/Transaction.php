<?php

namespace Redenge\EET\Model;

use Nette\Utils\Strings;
use Ondrejnov\EET\Utils\Format;
use Ondrejnov\EET\Utils\UUID;
use Redenge\EET\State;
use Redenge\ExchangeRate\ExchangeRate;

/**
 * Table/Object to store EET transactions
 */
class Transaction extends \DBObject// implements ITransaction
{
	const   REGULAR = 0,
			SIMPLIFIED = 1;

	/**
	 * @var string
	 */
	private $currency;

	/**
	 * @var ExchangeRate
	 */
	private $exchangeRate;

	/**
	 * @param \Database $database
	 * @param \DBObject $parentNode
	 */
	public function __construct($database = NULL, $parentNode = NULL)
	{
		$this->currency = \Redenge\EET\Settings::CURRENCY_DEFAULT;
		$this->exchangeRate = new ExchangeRate;

		if ($database) {
			parent::__construct($database, 'eet_transaction', $parentNode);
		} else {
			$this->createInteger('id');
		}

		parent::createObject('call', __NAMESPACE__ . '\Call');

		parent::createString('state', State::CREATED);
		parent::createString('email');
		parent::createString('uuid_zpravy', UUID::v4());
		parent::createDate('dat_odesl', '0000-00-00 00:00:00');
		parent::createBool('prvni_zaslani', TRUE);
		parent::createBool('overeni');
		parent::createString('dic_popl');
		parent::createString('dic_poverujiciho');
		parent::createString('id_provoz');
		parent::createString('id_pokl');
		parent::createString('porad_cis');
		parent::createDate('dat_trzby', (new \DateTime)->format('Y-m-d H:i:s'));
		parent::createFloat('celk_trzba', 0);
		parent::createFloat('zakl_nepodl_dph', 0);
		parent::createFloat('zakl_dan1', 0);
		parent::createFloat('dan1', 0);
		parent::createFloat('zakl_dan2', 0);
		parent::createFloat('dan2', 0);
		parent::createFloat('zakl_dan3', 0);
		parent::createFloat('dan3', 0);
		parent::createFloat('cest_sluz', 0);
		parent::createFloat('pouzit_zboz1', 0);
		parent::createFloat('pouzit_zboz2', 0);
		parent::createFloat('pouzit_zboz3', 0);
		parent::createFloat('urceno_cerp_zuct', 0);
		parent::createFloat('cerp_zuct', 0);
		parent::createBool('rezim', 0);
		parent::createString('pkp');
		parent::createString('bkp');
		parent::createDate('dat_prij', '0000-00-00 00:00:00');
		parent::createString('fik');
		parent::createBool('test');
		parent::createInteger('counter', 0);
		parent::createInteger('notify_sent', 0);
		parent::createString('language', \Redenge\EET\Settings::LANGUAGE_DEFAULT);

		parent::createInteger('id_order', 0);
		parent::createInteger('id_bill', 0);
	}

	/**
	 * @return int
	 */
	public function getId($filter = null)
	{
		return $this->id;
	}

	/**
	 * @param int
	 *
	 * @return Transaction
	 */
	public function setId($id)
	{
		$this->id = $id;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getBkp()
	{
		return $this->bkp;
	}

	/**
	 * @param mixed $bkp
	 *
	 * @return Transaction
	 */
	public function setBkp($bkp)
	{
		$this->bkp = $bkp;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCelkTrzba()
	{
		return Format::price($this->celk_trzba);
	}

	/**
	 * @param mixed $celk_trzba
	 *
	 * @return Transaction
	 */
	public function setCelkTrzba($celk_trzba)
	{
		$this->celk_trzba = $this->getPrice($celk_trzba);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCerpZuct()
	{
		return Format::price($this->cerp_zuct);
	}

	/**
	 * @param mixed $cerp_zuct
	 *
	 * @return Transaction
	 */
	public function setCerpZuct($cerp_zuct)
	{
		$this->cerp_zuct = $this->getPrice($cerp_zuct);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCestSluz()
	{
		return Format::price($this->cest_sluz);
	}

	/**
	 * @param mixed $cest_sluz
	 *
	 * @return Transaction
	 */
	public function setCestSluz($cest_sluz)
	{
		$this->cest_sluz = $this->getPrice($cest_sluz);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getCounter()
	{
		return $this->counter;
	}

	/**
	 * @param mixed $counter
	 *
	 * @return Transaction
	 */
	public function setCounter($counter)
	{
		$this->counter = $counter;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDan1()
	{
		return Format::price($this->dan1);
	}

	/**
	 * @param mixed $dan1
	 *
	 * @return Transaction
	 */
	public function setDan1($dan1)
	{
		$this->dan1 = $this->getPrice($dan1);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDan2()
	{
		return Format::price($this->dan2);
	}

	/**
	 * @param mixed $dan2
	 *
	 * @return Transaction
	 */
	public function setDan2($dan2)
	{
		$this->dan2 = $this->getPrice($dan2);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDan3()
	{
		return Format::price($this->dan3);
	}

	/**
	 * @param mixed $dan3
	 *
	 * @return Transaction
	 */
	public function setDan3($dan3)
	{
		$this->dan3 = $this->getPrice($dan3);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDatOdesl()
	{
		return $this->dat_odesl;
	}

	/**
	 * @param mixed $dat_odesl
	 *
	 * @return Transaction
	 */
	public function setDatOdesl($dat_odesl)
	{
		$this->dat_odesl = $dat_odesl;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDatPrij()
	{
		return $this->dat_prij;
	}

	/**
	 * @param mixed $dat_prij
	 *
	 * @return Transaction
	 */
	public function setDatPrij($dat_prij)
	{
		$this->dat_prij = $dat_prij;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDatTrzby($object = FALSE)
	{
		return $object ? new \DateTime($this->dat_trzby) : $this->dat_trzby;
	}

	/**
	 * @param mixed $dat_trzby
	 *
	 * @return Transaction
	 */
	public function setDatTrzby($dat_trzby)
	{
		$this->dat_trzby = $dat_trzby;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getDicPopl()
	{
		return empty($this->dic_popl) ? NULL : $this->dic_popl;
	}

	/**
	 * @param mixed $dic_popl
	 *
	 * @return Transaction
	 */
	public function setDicPopl($dic_popl)
	{
		$this->dic_popl = $dic_popl;
		return $this;
	}

	/**
	 * @return string|NULL
	 */
	public function getDicPoverujiciho()
	{
		return empty($this->dic_poverujiciho) ? NULL : $this->dic_poverujiciho;
	}

	/**
	 * @param mixed $dic_poverujiciho
	 *
	 * @return Transaction
	 */
	public function setDicPoverujiciho($dic_poverujiciho)
	{
		$this->dic_poverujiciho = $dic_poverujiciho;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getEmail()
	{
		return $this->email;
	}

	/**
	 * @param mixed $email
	 *
	 * @return Transaction
	 */
	public function setEmail($email)
	{
		$this->email = $email;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getFik()
	{
		return $this->fik;
	}

	/**
	 * @param mixed $fik
	 *
	 * @return Transaction
	 */
	public function setFik($fik)
	{
		$this->fik = $fik;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdBill()
	{
		return $this->id_bill;
	}

	/**
	 * @param mixed $id_bill
	 *
	 * @return Transaction
	 */
	public function setIdBill($id_bill)
	{
		$this->id_bill = $id_bill;
		if (!empty($id_bill)) {
			$this->state = State::BILL;
		}
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdOrder()
	{
		return $this->id_order;
	}

	/**
	 * @param mixed $id_order
	 *
	 * @return Transaction
	 */
	public function setIdOrder($id_order)
	{
		$this->id_order = $id_order;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdPokl()
	{
		return $this->id_pokl;
	}

	/**
	 * @param mixed $id_pokl
	 *
	 * @return Transaction
	 */
	public function setIdPokl($id_pokl)
	{
		$this->id_pokl = $id_pokl;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getIdProvoz()
	{
		return $this->id_provoz;
	}

	/**
	 * @param mixed $id_provoz
	 *
	 * @return Transaction
	 */
	public function setIdProvoz($id_provoz)
	{
		$this->id_provoz = $id_provoz;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getNotifySent()
	{
		return $this->notify_sent;
	}

	/**
	 * @param mixed $notify_sent
	 *
	 * @return Transaction
	 */
	public function setNotifySent($notify_sent)
	{
		$this->notify_sent = $notify_sent;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getOvereni()
	{
		return $this->overeni;
	}

	/**
	 * @param mixed $overeni
	 *
	 * @return Transaction
	 */
	public function setOvereni($overeni)
	{
		$this->overeni = $overeni;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPkp()
	{
		return $this->pkp;
	}

	/**
	 * @param mixed $pkp
	 *
	 * @return Transaction
	 */
	public function setPkp($pkp)
	{
		$this->pkp = $pkp;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPoradCis()
	{
		return $this->porad_cis;
	}

	/**
	 * @param mixed $porad_cis
	 *
	 * @return Transaction
	 */
	public function setPoradCis($porad_cis)
	{
		$this->porad_cis = $porad_cis;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPouzitZboz1()
	{
		return Format::price($this->pouzit_zboz1);
	}

	/**
	 * @param mixed $pouzit_zboz1
	 *
	 * @return Transaction
	 */
	public function setPouzitZboz1($pouzit_zboz1)
	{
		$this->pouzit_zboz1 = $this->getPrice($pouzit_zboz1);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPouzitZboz2()
	{
		return Format::price($this->pouzit_zboz2);
	}

	/**
	 * @param mixed $pouzit_zboz2
	 *
	 * @return Transaction
	 */
	public function setPouzitZboz2($pouzit_zboz2)
	{
		$this->pouzit_zboz2 = $this->getPrice($pouzit_zboz2);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPouzitZboz3()
	{
		return Format::price($this->pouzit_zboz3);
	}

	/**
	 * @param mixed $pouzit_zboz3
	 *
	 * @return Transaction
	 */
	public function setPouzitZboz3($pouzit_zboz3)
	{
		$this->pouzit_zboz3 = $this->getPrice($pouzit_zboz3);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getPrvniZaslani()
	{
		return $this->prvni_zaslani;
	}

	/**
	 * @param mixed $prvni_zaslani
	 *
	 * @return Transaction
	 */
	public function setPrvniZaslani($prvni_zaslani)
	{
		$this->prvni_zaslani = $prvni_zaslani;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getRezim()
	{
		return $this->rezim;
	}

	/**
	 * @param mixed $rezim
	 *
	 * @return Transaction
	 */
	public function setRezim($rezim)
	{
		$this->rezim = $rezim;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getState()
	{
		return $this->state;
	}

	/**
	 * @param mixed $state
	 *
	 * @return Transaction
	 */
	public function setState($state)
	{
		$this->state = $state;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getTest()
	{
		return $this->test;
	}

	/**
	 * @param mixed $test
	 *
	 * @return Transaction
	 */
	public function setTest($test)
	{
		$this->test = $test;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUrcenoCerpZuct()
	{
		return Format::price($this->urceno_cerp_zuct);
	}

	/**
	 * @param mixed $urceno_cerp_zuct
	 *
	 * @return Transaction
	 */
	public function setUrcenoCerpZuct($urceno_cerp_zuct)
	{
		$this->urceno_cerp_zuct = $this->getPrice($urceno_cerp_zuct);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getUuidZpravy()
	{
		return $this->uuid_zpravy;
	}

	/**
	 * @param mixed $uuid_zpravy
	 *
	 * @return Transaction
	 */
	public function setUuidZpravy($uuid_zpravy)
	{
		$this->uuid_zpravy = $uuid_zpravy;
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getZaklDan1()
	{
		return Format::price($this->zakl_dan1);
	}

	/**
	 * @param mixed $zakl_dan1
	 *
	 * @return Transaction
	 */
	public function setZaklDan1($zakl_dan1)
	{
		$this->zakl_dan1 = $this->getPrice($zakl_dan1);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getZaklDan2()
	{
		return Format::price($this->zakl_dan2);
	}

	/**
	 * @param mixed $zakl_dan2
	 *
	 * @return Transaction
	 */
	public function setZaklDan2($zakl_dan2)
	{
		$this->zakl_dan2 = $this->getPrice($zakl_dan2);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getZaklDan3()
	{
		return Format::price($this->zakl_dan3);
	}

	/**
	 * @param mixed $zakl_dan3
	 *
	 * @return Transaction
	 */
	public function setZaklDan3($zakl_dan3)
	{
		$this->zakl_dan3 = $this->getPrice($zakl_dan3);
		return $this;
	}

	/**
	 * @return mixed
	 */
	public function getZaklNepodlDph()
	{
		return Format::price($this->zakl_nepodl_dph);
	}

	/**
	 * @param mixed $zakl_nepodl_dph
	 *
	 * @return Transaction
	 */
	public function setZaklNepodlDph($zakl_nepodl_dph)
	{
		$this->zakl_nepodl_dph = $this->getPrice($zakl_nepodl_dph);
		return $this;
	}

	/**
	 * @param string
	 *
	 * @return Transaction
	 */
	public function setLanguage($language)
	{
		$this->language = Strings::lower($language);
		return $this;
	}

	/**
	 * @param string
	 */
	public function getLanguage()
	{
		return $this->language;
	}

	/**
	 * @param string $currency - kód měny podle ISO 4217 (třímístný kód)
	 *
	 * @return Transaction
	 */
	public function setCurrency($currency)
	{
		$currency = Strings::trim(Strings::upper($currency));
		if (Strings::length($currency) !== 3) {
			throw new \InvalidArgumentException('The length of the currency must be 3');
		}

		$this->currency = $currency;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCurrency()
	{
		return $this->currency ? $this->currency : \Redenge\EET\Settings::CURRENCY_DEFAULT;
	}

	/**
	 * Vrátí konečnou CZK cenu
	 * Pokud je transakce v jiné než CZK měně, tak provede přepočet podle kurzu ČNB
	 *
	 * @param float $price
	 * @return flaot
	 */
	private function getPrice($price)
	{
		$currencyIso = $this->getCurrency();
		if ($currencyIso === \Redenge\EET\Settings::CURRENCY_DEFAULT) {
			return Format::price($price);
		}

		$rate = $this->exchangeRate->getRate($currencyIso);
		return Format::price($price * $rate);
	}


	/**
	 * When FIK is available it returns FIK code, otherwise PKP code
	 *
	 * @return string
	 */
	public function getSecondaryCode()
	{
		return !empty($this->getFik()) ? $this->getFik() : $this->getPkp();
	}

	/**
	 * When FIK is available it returns 'FIK', otherwise 'PKP'
	 *
	 * @return string
	 */
	public function getSecondaryCodeName()
	{
		return !empty($this->getFik()) ? 'FIK' : 'PKP';
	}

	/**
	 * When FIK is available it returns dat_prij, otherwise dat_trzby
	 *
	 * @return string
	 */
	public function getSecondaryCodeDate()
	{
		return (new \DateTime($this->getDatTrzby()))->format('d. m. Y H:i:s');
	}

	/**
	 * @return string
	 */
	public function getFileName()
	{
		return sprintf('eet_%s%s.pdf', $this->getPoradCis(), $this->getIdBill() ? '_(B)' : '');
	}

	/**
	 * @return bool
	 */
	public function isSendable()
	{
		return $this->getBkp() && $this->getPkp();
	}

	/**
	 * @return string
	 */
	public function getFormattedRezim()
	{
		return $this->getRezimList($short = TRUE)[$this->getRezim()];
	}

	/**
	 * @param bool    $short
	 * @return array
	 */
	public static function getRezimList($short = FALSE)
	{
		return [
			self::REGULAR => 'Běžný' . ($short ? '' : ' režim'),
			self::SIMPLIFIED => 'Zjednodušený' . ($short ? '' : ' režim'),
		];
	}

	/**
	 * @param Transaction $transaction
	 *
	 * @return int|bool
	 */
	public function persist(Transaction $transaction)
	{
		if ($this->db === NULL) {
			throw new \Exception('Cannot call persist() method on \DBObject which is not attached to model.');
		}

		if ($transaction->getId() == 0) {
			$this->reset();
		}

		$this->id = $transaction->getId();
		$this->state = $transaction->getState();
		$this->email = $transaction->getEmail();
		$this->uuid_zpravy = $transaction->getUuidZpravy();
		$this->dat_odesl = $transaction->getDatOdesl();
		$this->prvni_zaslani = $transaction->getPrvniZaslani();
		$this->overeni = $transaction->getOvereni();
		$this->dic_popl = $transaction->getDicPopl();
		$this->dic_poverujiciho = $transaction->getDicPoverujiciho();
		$this->id_provoz = $transaction->getIdProvoz();
		$this->id_pokl = $transaction->getIdPokl();
		$this->porad_cis = $transaction->getPoradCis();
		$this->dat_trzby = $transaction->getDatTrzby();
		$this->celk_trzba = $transaction->getCelkTrzba();
		$this->zakl_nepodl_dph = $transaction->getZaklNepodlDph();
		$this->zakl_dan1 = $transaction->getZaklDan1();
		$this->dan1 = $transaction->getDan1();
		$this->zakl_dan2 = $transaction->getZaklDan2();
		$this->dan2 = $transaction->getDan2();
		$this->zakl_dan3 = $transaction->getZaklDan3();
		$this->dan3 = $transaction->getDan3();
		$this->cest_sluz = $transaction->getCestSluz();
		$this->pouzit_zboz1 = $transaction->getPouzitZboz1();
		$this->pouzit_zboz2 = $transaction->getPouzitZboz2();
		$this->pouzit_zboz3 = $transaction->getPouzitZboz3();
		$this->urceno_cerp_zuct = $transaction->getUrcenoCerpZuct();
		$this->cerp_zuct = $transaction->getCerpZuct();
		$this->rezim = $transaction->getRezim();
		$this->pkp = $transaction->getPkp();
		$this->bkp = $transaction->getBkp();
		$this->dat_prij = $transaction->getDatPrij();
		$this->fik = $transaction->getFik();
		$this->test = $transaction->getTest();
		$this->counter = $transaction->getCounter();
		$this->notify_sent = $transaction->getNotifySent();
		$this->language = $transaction->getLanguage();
		$this->currency = $transaction->getCurrency();
		$this->id_bill = $transaction->getIdBill();
		$this->id_order = $transaction->getIdOrder();

		return $this->save() ? $this->id : FALSE;
	}

	public function getStornoValues(array $values)
	{
		$change = [
			'id' => 0,
			'state' => State::CREATED,
			'uuid_zpravy' => UUID::v4(),
			'dat_odesl' => NULL,
			'prvni_zaslani' => 1,
			'dat_trzby' => (new \DateTime)->format('Y-m-d H:i:s'),
			'pkp' => NULL,
			'bkp' => NULL,
			'dat_prij' => NULL,
			'fik' => NULL,
			'counter' => 0,
			'notify_sent' => 0,
		];

		$negate = self::getStornoColumns();

		foreach ($change as $key => $value) {
			$values[$key] = $value;
		}

		foreach ($negate as $key) {
			$values[$key] = $values[$key] * -1;
		}

		return $values;
	}

	/**
	 * Create Transaction object from data or load by transaction.id
	 * @param int|array    $data id transaction or data
	 *
	 * @return Transaction
	 */
	public function get($data)
	{
		if (is_int($data)) {
			$this->load(['id' => (int) $data]);
			$data = $this;
		}

		return $this->fill($data);
	}

	/**
	 * @param array $conditions
	 *
	 * @return Transaction
	 */
	public function getBy(array $conditions)
	{
		$this->load($conditions);

		return $this->fill($this);
	}

	/**
	 * @param \StdClass|\DBObject $data
	 *
	 * @return Transaction
	 */
	private function fill($data)
	{
		if ($data->id === 0) {
			throw new \Exception('Transaction not found');
		}

		return (new Transaction())
			->setId($data->id)
			->setState($data->state)
			->setEmail($data->email)
			->setUuidZpravy($data->uuid_zpravy)
			->setDatOdesl($data->dat_odesl)
			->setPrvniZaslani($data->prvni_zaslani)
			->setOvereni($data->overeni)
			->setDicPopl($data->dic_popl)
			->setDicPoverujiciho($data->dic_poverujiciho)
			->setIdProvoz($data->id_provoz)
			->setIdPokl($data->id_pokl)
			->setPoradCis($data->porad_cis)
			->setDatTrzby($data->dat_trzby)
			->setCelkTrzba($data->celk_trzba)
			->setZaklNepodlDph($data->zakl_nepodl_dph)
			->setZaklDan1($data->zakl_dan1)
			->setDan1($data->dan1)
			->setZaklDan2($data->zakl_dan2)
			->setDan2($data->dan2)
			->setZaklDan3($data->zakl_dan3)
			->setDan3($data->dan3)
			->setCestSluz($data->cest_sluz)
			->setPouzitZboz1($data->pouzit_zboz1)
			->setPouzitZboz2($data->pouzit_zboz2)
			->setPouzitZboz3($data->pouzit_zboz3)
			->setUrcenoCerpZuct($data->urceno_cerp_zuct)
			->setCerpZuct($data->cerp_zuct)
			->setRezim($data->rezim)
			->setPkp($data->pkp)
			->setBkp($data->bkp)
			->setDatPrij($data->dat_prij)
			->setFik($data->fik)
			->setTest($data->test)
			->setCounter($data->counter)
			->setNotifySent($data->notify_sent)
			->setLanguage($data->language)
			->setIdBill($data->id_bill)
			->setIdOrder($data->id_order);
	}

	/**
	 * @return array
	 */
	public static function getStornoColumns()
	{
		return [
			'celk_trzba',
			'zakl_nepodl_dph',
			'zakl_dan1',
			'dan1',
			'zakl_dan2',
			'dan2',
			'zakl_dan3',
			'dan3',
			'cest_sluz',
			'pouzit_zboz1',
			'pouzit_zboz2',
			'pouzit_zboz3',
			'urceno_cerp_zuct',
			'cerp_zuct',
		];
	}
}
