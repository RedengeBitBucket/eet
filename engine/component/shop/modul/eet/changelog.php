<?php

$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', basename(__FILE__, '.php')));
$template->prepare();

$changelogFile = __DIR__ . '/../../../../../CHANGELOG.md';
if (file_exists($changelogFile)) {
	$markdown = new Parsedown();
	$changelog = $markdown->text(file_get_contents($changelogFile));
} else {
	$changelog = 'Changelog nebyl nalezen :-(';
}

$template->assign('CHANGELOG', nl2br($changelog));

$templateLevel1->assign('BODY', $template->getOutputContent());
