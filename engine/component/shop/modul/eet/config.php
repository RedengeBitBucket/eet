<?php

use Nette\Forms\Form;
use Nette\Utils\Html;
use Redenge\Admin\Module\EET;

$template = new \TemplatePower(sprintf('%s/%s/%s.tpl', __DIR__, 'templates', basename(__FILE__, '.php')));
$template->prepare();

$form = new Form;
$form->addSelect('eet_dispatcher', 'Poskytovatel', \Redenge\EET\Settings::getDispatcherList())
	->setRequired();
$form->addText('eet_max_send_attempts', 'Počet pokusů o odeslání')
	->setRequired()
	->addRule(Form::INTEGER)
	->addRule(Form::MIN, 'Počet pokusů o odeslání musí být >= %d', 2);
$form->addCheckbox('eet_active', 'Aktivní');
$form->addCheckbox('eet_production', 'Ostrý provoz');
$form->addCheckbox('eet_overeni', 'Ověřovací mód odeslání');
$form->addCheckbox('eet_no_email_send', 'Neodesílat email na zákazníka');
$form->addText('eet_dic_popl', 'DIČ poplatníka')
	->setRequired();
$form->addText('eet_dic_poverujiciho', 'DIČ pověřujícího poplatníka');
$form->addText('eet_id_provoz', 'Označení provozovny')
	->setRequired();
$form->addText('eet_id_pokl', 'Označení pokladního zařízení')
	->setRequired();
$form->addCheckbox('eet_rezim', _('Zjednodušený režim')) // Režim tržby [0 = bezny, 1 => zjednoduseny]
	->setDisabled();
$form->addText('eet_email', 'Email pro chyby')
	->setRequired()
	->addRule(Form::EMAIL);
$form->addText('eet_cert', 'Certifikát (WNI)')
	->addConditionOn($form['eet_dispatcher'], Form::EQUAL, \Redenge\EET\Settings::WNI_DISPATCHER)
		->setRequired();
$form->addText('eet_password', 'Heslo (WNI)');
$form->addText('eet_api_production', 'API')
	->addConditionOn($form['eet_dispatcher'], Form::EQUAL, \Redenge\EET\Settings::WNI_DISPATCHER)
		->setRequired();
$form->addText('eet_api_development', 'API (DEV)')
	->addConditionOn($form['eet_dispatcher'], Form::EQUAL, \Redenge\EET\Settings::WNI_DISPATCHER)
		->setRequired();
$form->addText(\Redenge\EET\Settings::LANGUAGE_KEY, 'Jazyky')
	->setAttribute('placeholder', \Redenge\EET\Settings::LANGUAGE_DEFAULT);
$form->addSelect('eet_cron_status', 'CRON status', \Redenge\EET\CronStatus::getList(TRUE))
	->setDisabled();

$form->addSubmit('send', _('Uložit'));

/**
 * Výchozí hodnoty
 */
$defaults = [];
$defaults['eet_cron_status'] = $settings->getCronStatus();

$records = $engine->shop->settings->getRecords('*', NULL, NULL, sprintf('%s = %d', $themeColumnName, \Redenge\Admin\Module\EET::SETTINGS_THEME));
while ($row = mysqlc_fetch_assoc($records)) {
	$defaults[$row['code']] = $row['setting'];
}
mysqlc_free_result($records);

$form->setDefaults($defaults);

/**
 * Odeslání formuláře
 */
if ($form->isSuccess()) {
	$values = $form->getValues();

	foreach ($values as $key => $value) {
		$engine->shop->settings->{$loadMethodName}([
			'code' => $key,
			$themeColumnName => EET::SETTINGS_THEME,
		]);

		if ($engine->shop->settings->id === 0) {
			$engine->shop->settings->{$resetMethodName}();
			$engine->shop->settings->code = $key;
			$engine->shop->settings->{$themeColumnName} = EET::SETTINGS_THEME;
			$engine->shop->settings->type = 'text'; // @todo
		}

		$engine->shop->settings->setting = $value;
		$engine->shop->settings->save();
	}
}

$template->assign('FORM', (string) $form);

$templateLevel1->assign('BODY', $template->getOutputContent());
